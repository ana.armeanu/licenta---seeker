﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public static Action PlayerHasMoved = delegate { };

    [SerializeField]
    private FixedJoystick movementJoystick;
    [SerializeField]
    private Button jumpButton;
    [SerializeField]
    private float jumpHeight = 8f;
    [SerializeField]
    private float rotationSpeed = 6f;
    [SerializeField]
    private float moveSpeed = 9f;

    private Rigidbody body;
    private Animator anim;
    private bool isGrounded;
    private bool clicked;
    private List<GameObject> currentColliders;
    private AudioSource walkingAudio;
   

    //! Disable "Apply root motion" on the Animator
    private void Start()
    {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        currentColliders = new List<GameObject>();
        jumpButton.onClick.AddListener(Clicked);
    }

    private void Clicked()
    {
        clicked = true;
    }

    private void Update()
    {
        bool isJumping = false;
        if (clicked && isGrounded)
        {
            body.velocity += jumpHeight * Vector3.up;
            isJumping = true;
            clicked = false;
        }
        anim.SetBool("IsJumping", isJumping);
        clicked = false;
        float vertMovement = movementJoystick.Vertical;
        float horMovement = movementJoystick.Horizontal;

        bool isWalking;
        if (horMovement != 0f || vertMovement != 0f)
        {
            PlayerHasMoved();
            isWalking = isGrounded;
            //rotate the body in the direction of the velocity (the move direction)
            if (vertMovement != 0)
            {
                transform.Translate(transform.forward * Time.deltaTime * moveSpeed * vertMovement, Space.World);
            }

            //Rotate Left/Right
            if (horMovement != 0)
            {
                transform.Rotate(new Vector3(0, 14, 0) * Time.deltaTime * rotationSpeed * horMovement, Space.Self);
            }

        }
        else
        {
            isWalking = false;
        }
        anim.SetBool("IsWalking", isWalking);
        if (isWalking)
        {
            if (walkingAudio == null)
            {
                walkingAudio = SoundManager.PlaySoundOnce(SoundManager.SoundName.PlayerMove, gameObject);
            }
            else
            {
                if(walkingAudio.isPlaying == false)
                    walkingAudio = SoundManager.PlaySoundOnce(SoundManager.SoundName.PlayerMove, gameObject);
            }
        }
    }

    void OnCollisionEnter(Collision theCollision)
    {
        if (theCollision.gameObject.tag == "Floor")
        {
            isGrounded = true;
        }
        currentColliders.Add(theCollision.gameObject);
    }

    void OnCollisionExit(Collision theCollision)
    {
        currentColliders.Remove(theCollision.gameObject);
        bool found = false;
        foreach (GameObject obj in currentColliders)
        {
            if (obj.tag == "Floor")
            {
                found = true;
                break;
            }
        }
        if (!found)
        {
            isGrounded = false;
        }
    }
}
