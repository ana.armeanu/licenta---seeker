﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCameraController : MonoBehaviour
{
    [SerializeField]
    private Camera closeCamera;
    [SerializeField]
    private Camera farCamera;
    [SerializeField]
    private Button switchCamera;

    private void OnEnable()
    {
       closeCamera.gameObject.SetActive(true);
       farCamera.gameObject.SetActive(false);

       switchCamera.onClick.AddListener(SwitchCameras);
    }

    private void SwitchCameras()
    {
        if (closeCamera.isActiveAndEnabled)
        {
            farCamera.gameObject.SetActive(true);
            closeCamera.gameObject.SetActive(false);
        }
        else
        {
            farCamera.gameObject.SetActive(false);
            closeCamera.gameObject.SetActive(true);
        }
    }

    public Camera GetActiveCamera()
    {
        if (closeCamera.isActiveAndEnabled)
            return closeCamera;
        return farCamera;
    }

    private void OnDisable()
    {
        switchCamera.onClick.RemoveListener(SwitchCameras);
    }

}
