﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsHandler : BaseStatsHandler
{
    [SerializeField]
    private Image manaBar;
    [SerializeField]
    private Canvas playerInputCanvas;
    [SerializeField]
    private Transform loseMenu;
    private Animator anim;
    private bool hasDied = false;

    private int manaValue = 100;

    private void Start()
    {
        // BaseCollectableInfo.StartedReadingCollectable += StopMovement;
        //BaseCollectableInfo.StartedReadingCollectable += DeactivatePlayerInput;
        //ButtonInteraction.FinishedReadingCollectable += ActivatePlayerInput;
        loseMenu.gameObject.SetActive(false);
        anim = GetComponent<Animator>();
        // ActivatePlayerInput();
    }

    public override void DecreaseHealth(float damage)
    {
        if (healthValue > 0)
        {
            SoundManager.PlaySoundOnce(SoundManager.SoundName.PlayerGotHit, gameObject, 1.5f);
            StartCoroutine(ResetDamageAnimation());
            base.DecreaseHealth(damage);
        }
    }

    private IEnumerator ResetDamageAnimation()
    {
        anim.SetBool("Damage", true);
        yield return new WaitForSeconds(1f);
        anim.SetBool("Damage", false);
    }

    private void OnEnable()
    {
        StartCoroutine(IncreaseManaOverTime());
    }

    void Update()
    {
        CheckFallToDeath();
        if (healthValue <= 0 && !hasDied)
        {
            hasDied = true;
            StartCoroutine(DeathProcedures());
        }
    }

    private void CheckFallToDeath()
    {
        if (transform.position.y <= -2)
        {
            healthValue = 0;
        }
    }

    private IEnumerator DeathProcedures()
    {
        anim.SetBool("HasDied", true);
        DeactivatePlayerInput();
        yield return new WaitForSeconds(2);
        loseMenu.gameObject.SetActive(true);
        SoundManager.PlaySoundOnce(SoundManager.SoundName.SoloGameOver);
    }

    private void ActivatePlayerInput()
    {
        playerInputCanvas.gameObject.SetActive(true);

    }

    private void DeactivatePlayerInput()
    {
        IEnumerable<FixedJoystick> joysticks = playerInputCanvas.gameObject.GetComponents<FixedJoystick>();
        foreach (FixedJoystick joystick in joysticks)
        {
            joystick.gameObject.SetActive(false);
        }
        playerInputCanvas.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        ButtonInteraction.FinishedReadingCollectable -= ActivatePlayerInput;
        //BaseCollectableInfo.StartedReadingCollectable -= DeactivatePlayerInput;
    }

    //Once every 3 seconds add 5/100 mana
    private IEnumerator IncreaseManaOverTime()
    {
        yield return new WaitForSeconds(3);
        AddMana(5);
        StartCoroutine(IncreaseManaOverTime());
    }

    public void AddMana(int mana)
    {
        if (manaValue < 100)
        {
            manaValue = Mathf.Min(100, manaValue + mana);
            UpdateManaBar();
        }
    }

    public void AddHealth(int health)
    {
        if (healthValue < 100)
        {
            healthValue = Mathf.Min(100, healthValue + health);
            UpdateHealthBar();
        }
    }

    public void DecreaseMana(int mana)
    {
        manaValue = Mathf.Max(0, manaValue - mana);
        UpdateManaBar();
    }

    public bool CheckMana(int manaPrice)
    {
        return manaValue >= manaPrice;
    }

    private void UpdateManaBar()
    {
        manaBar.fillAmount = Mathf.Max(0, manaValue * multiplier);
    }
}
