﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    private Rigidbody body;
    private bool isGrounded;
    private List<GameObject> currentColliders;

    void Start()
    {
        transform.gameObject.SetActive(true);
        body = GetComponent<Rigidbody>();
        currentColliders = new List<GameObject>();
    } 

    void OnCollisionEnter(Collision theCollision)
    {
        if (theCollision.gameObject.tag == "Floor")
        {
            isGrounded = true;
        }
        currentColliders.Add(theCollision.gameObject);
    }

    void OnCollisionExit(Collision theCollision)
    {
        currentColliders.Remove(theCollision.gameObject);
        bool found = false;
        foreach (GameObject obj in currentColliders)
        {
            if (obj.tag == "Floor")
            {
                found = true;
                break;
            }
        }
        if (!found)
        {
            isGrounded = false;
        }
    }
}
