﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static SoloPlayLevelManager;

//Level 3
public class AstroLevelHandler : LevelHandler
{
    private void Awake()
    {
        minInfoNb = 7;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        Instance.LevelLoaded(Level.AstronomyLevel, sceneIndex);
    }
}