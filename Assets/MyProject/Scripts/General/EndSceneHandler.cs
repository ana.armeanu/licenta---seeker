﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static SoloPlayLevelManager;

public class EndSceneHandler : MonoBehaviour
{
    [SerializeField]
    private Button mainMenu;
    // Start is called before the first frame update
    void OnEnable()
    {
        mainMenu.onClick.AddListener(GoToMainMenu);
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        Instance.LevelLoaded(Level.Ending, sceneIndex);
    }

    private void GoToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
