﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageGiver : MonoBehaviour
{
    [SerializeField]
    protected int damage;
    private bool hasGivenDamage = false;

    private void Start()
    {
        ;
    }

    protected void OnTriggerEnter(Collider collider)
    {
         if (collider.tag == "Player")
         {
                PlayerStatsHandler statsHandler = collider.gameObject.GetComponent<PlayerStatsHandler>();
                statsHandler.DecreaseHealth(damage);
                DeactivateGameObject();
         }
         else
         {
             if (collider.tag == "Enemy")
             {
                EnemyStatsHandler statsHandler = collider.gameObject.GetComponent<EnemyStatsHandler>();
                statsHandler.DecreaseHealth(damage);
                DeactivateGameObject();
             }
         }
    }

    private void DeactivateGameObject()
    {
        this.gameObject.SetActive(false);
    }
}
