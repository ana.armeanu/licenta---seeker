﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : DamageGiver
{
    [SerializeField]
    private Transform shooter;
    [SerializeField]
    protected float range = 100f;
    [SerializeField]
    protected Button shootButton;
    [SerializeField]
    protected Transform weaponObject;
    [SerializeField]
    protected ParticleSystem shootFlash;
    [SerializeField]
    protected GameObject hitFlash;
    // how much mana will be lost/shot 
    private int manaPrice = 10;
    private PlayerStatsHandler playerStatus;
    private Animator anim;

    protected void Start()
    {
        playerStatus = shooter.gameObject.GetComponent<PlayerStatsHandler>();
        anim = shooter.gameObject.GetComponent<Animator>();
    }

    private void OnEnable()
    {
        shootButton.onClick.AddListener(Shoot);
    }

    protected void Shoot()
    {
        if (playerStatus.CheckMana(manaPrice))
        {
            anim.SetBool("IsAttacking", true);
            SoundManager.PlaySoundOnce(SoundManager.SoundName.PlayerAttack, gameObject);
            if (shootFlash != null)
            {
                shootFlash.Play();
            }
            DecreaseMana();

            RaycastHit hitInfo;
            //from the camera position, in the forward direction, collect info in hitinfo
            if (Physics.Raycast(weaponObject.position, weaponObject.forward, out hitInfo, range))
            {
                ShowHitParticles(hitInfo);
                //get the status handler of the object hit and decrease its health 
                BaseStatsHandler hitStatus = hitInfo.transform.gameObject.GetComponent<BaseStatsHandler>();
                if (hitStatus != null)
                {
                    hitStatus.DecreaseHealth(damage);
                }
            }
            StartCoroutine(ResetAnim());
        }
    }

    IEnumerator ResetAnim()
    {
        yield return new WaitForSeconds(0.5f);
        anim.SetBool("IsAttacking", false);
    }


    private void DecreaseMana()
    {
        playerStatus.DecreaseMana(manaPrice);
    }

    private void ShowHitParticles(RaycastHit hitInfo)
    {
        GameObject hitFlashInst = Instantiate(hitFlash, hitInfo.point, Quaternion.LookRotation(hitInfo.normal));
        Destroy(hitFlashInst, 2f);
    }

    private void OnDisable()
    {
        shootButton.onClick.RemoveListener(Shoot);
    }
}
