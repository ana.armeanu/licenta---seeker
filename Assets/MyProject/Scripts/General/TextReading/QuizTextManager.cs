﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizTextManager : MonoBehaviour
{
    public static List<QuizQA> ReadPiecesOfInfo(string path)
    {
        List<string> informationList = new List<string>();
        string allText;

        TextAsset t = Resources.Load(path) as TextAsset;
        allText = t.text;

        List<QuizQA> parsedTextList = ParseQAText(allText);

        return parsedTextList;
    }

    private static List<QuizQA> ParseQAText(string allText)
    {
        if (!String.IsNullOrEmpty(allText))
        {
            string[] fields = allText.Split(new string[] { "\r\n***___***\r\n" }, StringSplitOptions.None);
            List<QuizQA> list = new List<QuizQA>();
            foreach (string s in fields)
            {
                string[] textParts = s.Split(new string[] { "\r\n~~~\r\n" }, StringSplitOptions.None);
                QuizQA quiz = new QuizQA();
                quiz.Question = textParts[0];
                quiz.Answer = textParts[1];
                quiz.Wrong1 = textParts[2];
                quiz.Wrong2 = textParts[3];
                quiz.Wrong3 = textParts[4];
                list.Add(quiz);
            }
            return list;
        }
        return null;
    }
}
