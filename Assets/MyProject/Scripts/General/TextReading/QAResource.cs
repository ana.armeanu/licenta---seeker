﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QAResource
{
    private readonly string infoPath = "QuizQA";
    protected List<QuizQA> unusedQA = new List<QuizQA>();
    protected List<QuizQA> usedQA = new List<QuizQA>();

    QAResource() {
        unusedQA = QuizTextManager.ReadPiecesOfInfo(infoPath);
    }

    public QuizQA GetRandomInfo()
    {
        if (unusedQA.Count == 0)
        {
            RenewUnused();
        }
        System.Random rnd = new System.Random();
        int idx = rnd.Next(0, unusedQA.Count);
        QuizQA show = unusedQA[idx];
        usedQA.Add(unusedQA[idx]);
        unusedQA.RemoveAt(idx);
        return show;
    }

    private void RenewUnused()
    {
        foreach (var qa in usedQA)
        {
            unusedQA.Add(qa);
        }

        usedQA.Clear();
    }
}
