﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizQA
{
    public string Question;
    public string Answer;
    public string Wrong1;
    public string Wrong2;
    public string Wrong3;
}
