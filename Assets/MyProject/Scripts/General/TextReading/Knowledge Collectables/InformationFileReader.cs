﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Collectables
{
    public static class InformationFileReader
    {
        public static List<string> ReadPiecesOfInfo(string path)
        {
            List<string> informationList = new List<string>();
            string allText;

            /* //doesn't work on Unity build
            StreamReader reader = new StreamReader(path);
            allText = reader.ReadToEnd();
            Debug.Log(allText);
            reader.Close();
            */

            TextAsset t = Resources.Load(path) as TextAsset;
            allText = t.text;

            List<string> parsedTextList = ParseText(allText);

            return parsedTextList;
        }

        private static List<string> ParseText(string allText)
        {
            if (!String.IsNullOrEmpty(allText))
            {
                string[] fields = allText.Split(new string[] { "\r\n***___***\r\n" }, StringSplitOptions.None);
                List<string> list = new List<string>();
                foreach (string s in fields)
                {
                    list.Add(s);
                }
                return list;
            }
            return null;
        }
    }
}