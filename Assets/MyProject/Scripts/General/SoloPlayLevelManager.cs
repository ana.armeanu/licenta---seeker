﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoloPlayLevelManager : MonoBehaviour
{
    private static SoloPlayLevelManager instance;

    private Level currentLevel;

    public enum Level
    {
        WaterLevel, 
        EarthLevel, 
        AstronomyLevel,
        Instructions,
        Ending
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public static SoloPlayLevelManager Instance
    {
        get
        {
            if (instance == null)
                instance = Instantiate(Resources.Load<SoloPlayLevelManager>("SoloPlayLevelManager"));
            return instance;
        }
    }

    public void LoadNextLevel()
    {
        switch (currentLevel)
        {
            case Level.WaterLevel:
                {
                    SceneManager.LoadScene("Earth Scene");
                    break;
                }

            case Level.EarthLevel:
                {
                    SceneManager.LoadScene("Astronomy Scene");
                    break;
                }

            case Level.AstronomyLevel:
                {
                    SceneManager.LoadScene("Final Scene");
                    break;
                }
        }

    }

    public void LevelLoaded(Level lvl, int sceneIndex)
    {
        PlayerPrefs.SetInt("PlayedScene", sceneIndex);
        currentLevel = lvl;
        switch (lvl)
        {
            case Level.Instructions:
            {
                SoundManager.PlaySoundLoop(SoundManager.SoundName.InstructionsMusic);
                break;
            }
            
            case Level.WaterLevel:
                {
                    SoundManager.PlaySoundLoop(SoundManager.SoundName.LevelMusic);
                    break;
                }

            case Level.EarthLevel:
                {
                    SoundManager.PlaySoundLoop(SoundManager.SoundName.Level2Music);
                    break;
                }

            case Level.AstronomyLevel:
                {
                    SoundManager.PlaySoundLoop(SoundManager.SoundName.Level3Music);
                    break;
                }
    
            case Level.Ending:
                {
                    SoundManager.PlaySoundLoop(SoundManager.SoundName.EndSceneMusic);
                    SoundManager.PlaySoundOnce(SoundManager.SoundName.SoloWon);
                    break;
                }

            default: break;       
        }
    }

}
