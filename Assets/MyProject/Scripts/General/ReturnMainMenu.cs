﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ReturnMainMenu : MonoBehaviour
{
    [SerializeField]
    private RectTransform mainCanvas;
    [SerializeField]
    private Button mainMenuBtn;
    [SerializeField]
    private RectTransform surePanel;
    [SerializeField]
    private Button yesBtn;
    [SerializeField]
    private Button noBtn;

    void OnEnable()
    {
        mainMenuBtn.onClick.AddListener(ActivatePanel);
        yesBtn.onClick.AddListener(GoToMainMenu);
        noBtn.onClick.AddListener(ReturnToGame);
        surePanel.gameObject.SetActive(false);
    }

    void OnDisable()
    {
        mainMenuBtn.onClick.RemoveAllListeners();
        yesBtn.onClick.RemoveAllListeners();
        noBtn.onClick.RemoveAllListeners();
    }

    private void ActivatePanel()
    {
        surePanel.gameObject.SetActive(true);
        if (mainCanvas != null)
        {
            mainCanvas.gameObject.SetActive(false);
        }
    }

    private void GoToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    private void ReturnToGame()
    {
        if (mainCanvas != null)
        {
            mainCanvas.gameObject.SetActive(true);
        }
        surePanel.gameObject.SetActive(false);
    }
}
