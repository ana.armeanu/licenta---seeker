﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//singleton class that will be injected by Zenject
public class Account
{
    public static Action GotDatabaseXPPoints = delegate { };
    private readonly string defaultUsername = "Not signed in";
    private string username;
    private string email;
    private int userXP;
    private int userLevel;

    public int UserXP { get { return userXP; } set { userXP = value; } }
    public int UserLevel { get { return userLevel; } set { userLevel = value; } }

    public string Username
    {
        get { return username; }
        set { username = string.Copy(value); ;  }
    }

    public string Email
    {
        get { return email; }
        set { email = string.Copy(value); ; }
    }

    public Account()
    {
        username = defaultUsername;
        email = defaultUsername;
        userXP = 0;
        userLevel = 0;
    }


    public void SetAccountDefault()
    {
       username = "Not signed in";
       email = "Not signed in";
       userXP = 0;
       userLevel = 0;

    }
    /// <summary>
    /// Set the email and username
    /// If the user is logged in, get the xp info from the database
    /// </summary>
    public void SetAccount(string emailReceived)
    {
        Email = string.Copy(emailReceived);
        Username = ExtractUserName(emailReceived);
        if (string.Equals(defaultUsername, username) == false)
        {
            SetXPAccount();
        }
    }

    public void SetXPAccount()
    {
        string id = Email.Replace("@", "");
        id = id.Replace(".", "");

        FirebaseDatabase.DefaultInstance
                .GetReference($"users/{id}")
                .GetValueAsync()
                .ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Debug.Log("Fail To Load");
                    }
                    else if (task.IsCompleted)
                    {
                        //Oare e pe threadul bun?
                        DataSnapshot snapshot = task.Result;

                        foreach (DataSnapshot h in snapshot.Children)
                        {
                            if (String.Equals(h.Key, "Experience"))
                            {
                                UserXP = Int32.Parse(h.Value.ToString());
                                GotDatabaseXPPoints();
                                break;
                            }
                        }
                    }
                }
            , System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
    }

    private class NestedUser
    {
        public string Username;
        public int Experience;
    }

    /// <summary>
    /// If the player is signed in, try and update the database
    /// Otherwise, update only the UI
    /// </summary>
    public void UpdateXP(int xp)
    {
        UserXP += xp;

        if (string.Equals(defaultUsername, username) == false)
        {
            string id = email.Replace("@", "");
            id = id.Replace(".", "");
            NestedUser user = new NestedUser();
            user.Username = username;
            user.Experience = UserXP;
            string json = JsonUtility.ToJson(user);

            //use the email as a unique identifier
            var playerDbRef = FirebaseDatabase.DefaultInstance.RootReference;
            playerDbRef.Child("users").Child(id).SetRawJsonValueAsync(json);
            GotDatabaseXPPoints();
        }
    }

    private string ExtractUserName(string email)
    {
        int idx = email.IndexOf("@");
        return email.Substring(0, idx);
    }
}
