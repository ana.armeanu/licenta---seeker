﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AccountFirebaseHandler : IInitializable
{
    protected Firebase.Auth.FirebaseAuth auth;
    protected Firebase.Auth.FirebaseAuth otherAuth;
    protected Dictionary<string, Firebase.Auth.FirebaseUser> userByAuth = new Dictionary<string, Firebase.Auth.FirebaseUser>();

    public static Action OnSignedIn = delegate { };
    public static Action OnSignedUp = delegate { };
    public static Action OnErrorOccured = delegate { };

    AccountFirebaseHandler()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            }
        });
    }


    public void Initialize()
    {

        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://seeker-66515.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
    }



    private void InitializeFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        if (auth.CurrentUser != null)
        {
            //SetAccountPersonalized(auth.CurrentUser.Email);
        }
    }

    public Firebase.Auth.FirebaseAuth GetAuth()
    {
        return auth;
    }


    public async System.Threading.Tasks.Task SignUpAsync(string email, string password)
    {
        if (!ValidEmail(email))
        {
            OnErrorOccured();
            return;
        }

        try { 

            await auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                // Firebase user has been created.
                Firebase.Auth.FirebaseUser newUser = task.Result;

                Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);

                //Add the user info to the database
                AddNewAccountDb(newUser.Email);
            });
        }
        catch (Exception exception)
        {
            Debug.LogWarning(exception);
            OnErrorOccured();
            return;
        }

        OnSignedUp();
    }

    private class NestedUser
    {
        public string Username;
        public int Experience;
    }

    public void AddNewAccountDb(string username)
    {
        string id = username.Replace("@", "");
        id = id.Replace(".", "");

        NestedUser user = new NestedUser();
        user.Username = username;
        user.Experience = 0;
        string json = JsonUtility.ToJson(user);
        var playerDbRef = FirebaseDatabase.DefaultInstance.RootReference;
        playerDbRef.Child("users").Child(id).SetRawJsonValueAsync(json);
    }


    public async System.Threading.Tasks.Task SignInAsync(string email, string password)
    {
        if (!ValidEmail(email))
        {
            OnErrorOccured();
            return;
        }

        try
        {
            await auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                Firebase.Auth.FirebaseUser newUser = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
            });
        }
        catch (Exception exception)
        {
            Debug.LogWarning(exception);
            OnErrorOccured();
            return;
        }

        OnSignedIn();
    }


    public Firebase.Auth.FirebaseUser GetUser()
    {
        return auth.CurrentUser;
    }


    public bool ValidEmail(string email)
    {
        if (String.IsNullOrEmpty(email)) return false;
        int idxAt = email.IndexOf("@");
        int idxDot = email.LastIndexOf(".");
        if (idxAt == -1 || idxDot == -1) return false;
        if (idxAt > 0 && idxDot - idxAt > 1 && idxDot < (email.Length - 1))
            return true;
        return false;
    }

    public bool CheckIfSignedIn()
    {
        if (auth!= null)
        {
            Firebase.Auth.FirebaseUser user = GetUser();
            if (user != null)
            {
                return true;
            }
        }
        return false;
    }

}
