﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;
using System;

public class UserDetails : MonoBehaviour
{
    [SerializeField]
    private RectTransform panel;
    [SerializeField]
    private RectTransform xpBackground;
    [SerializeField]
    private TextMeshProUGUI xp;
    [SerializeField]
    private TextMeshProUGUI username;

    private Account account;
    private AccountFirebaseHandler firebaseHandler;

    [Inject]
    private void Init(Account injectedAccount, AccountFirebaseHandler injectedFirebaseHandler)
    {
        account = injectedAccount;
        firebaseHandler = injectedFirebaseHandler;
    }

    private void Start()
    {
        Account.GotDatabaseXPPoints += RefreshXP;
        AccountMenuHandler.OnRegistered += UpdateAccountDetails;
        AccountMenuHandler.OnRegistered += ShowAccountDetails;
        AccountMenuHandler.OnSignedIn += UpdateAccountDetails;
        AccountMenuHandler.OnSignedIn += ShowAccountDetails;
        AccountMenuHandler.OnSignedOut += UpdateAccountDetails;
        AccountMenuHandler.OnSignedOut += HideAccountDetails;
    }

    private void RefreshXP()
    {
        xp.text = account.UserXP.ToString() + " XP";
        xp.ForceMeshUpdate();
    }

    private void OnEnable()
    {
        panel.gameObject.SetActive(false);
        StartCoroutine(EnableCheckSignInProcess());
    }

    private IEnumerator EnableCheckSignInProcess()
    {
        yield return new WaitForSeconds(1f);

        if (firebaseHandler.CheckIfSignedIn())
        {
            account.SetAccount(firebaseHandler.GetUser().Email);
            UpdateAccountDetails();
            ShowAccountDetails();
        }
        else
        {
            account.SetAccountDefault();
            SetUsername();
            HideAccountDetails();
        }
        panel.gameObject.SetActive(true);
    }

    private void UpdateAccountDetails()
    {
        SetUsername();
        SetAccountDetails();
    }

    private void SetAccountDetails()
    {
        xp.text = account.UserXP.ToString() + " XP";
    }

    public void SetUsername()
    {
       username.text = account.Username;
    }


    private void ShowAccountDetails()
    {
        SetAccountDetails();
        xp.gameObject.SetActive(true);
        xpBackground.gameObject.SetActive(true);
    }

    private void HideAccountDetails()
    {
        xp.gameObject.SetActive(false);
        xpBackground.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Account.GotDatabaseXPPoints -= RefreshXP;
        AccountMenuHandler.OnSignedIn -= ShowAccountDetails;
        AccountMenuHandler.OnSignedOut -= HideAccountDetails;
        AccountMenuHandler.OnSignedIn -= UpdateAccountDetails;
        AccountMenuHandler.OnSignedOut -= UpdateAccountDetails;
    }
}
