﻿using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ResetPassword : MonoBehaviour
{
    private readonly string notAnEmailMessage = "The email is not valid. Try again.";
    private readonly string errorMessage = "Something went wrong.";

    [SerializeField]
    private Button sendEmailBtn;
    [SerializeField]
    private Button resetPasswordBtn;
    [SerializeField]
    private Button closeEmailSentPanel;
    [SerializeField]
    private Button closeSendEmailPanel;
    [SerializeField]
    private RectTransform emailSentPanel;
    [SerializeField]
    private RectTransform sendEmailPanel;
    [SerializeField]
    private TMP_InputField emailField;
    [SerializeField]
    private TextMeshProUGUI errorText;
    [SerializeField]
    private TextMeshProUGUI waitText;


    AccountFirebaseHandler firebaseHandler;
    private string email;
    private bool emailExists = false;

    [Inject]
    private void Init(AccountFirebaseHandler injectedFirebaseHandler)
    {
        firebaseHandler = injectedFirebaseHandler;
    }

    private void OnEnable()
    {

        waitText.gameObject.SetActive(false);
        errorText.gameObject.SetActive(false);
        emailSentPanel.gameObject.SetActive(false);
        sendEmailPanel.gameObject.SetActive(false);
        resetPasswordBtn.onClick.AddListener(() => { sendEmailPanel.gameObject.SetActive(true); });
        sendEmailBtn.onClick.AddListener(async () => { await ResetPasswordMethodAsync(); });
        closeEmailSentPanel.onClick.AddListener(() => { emailSentPanel.gameObject.SetActive(false); });
        closeSendEmailPanel.onClick.AddListener(() => { sendEmailPanel.gameObject.SetActive(false); });
    }

    private void OnDisable()
    {
        sendEmailBtn.onClick.RemoveAllListeners();
        resetPasswordBtn.onClick.RemoveAllListeners();
        closeEmailSentPanel.onClick.RemoveAllListeners();
        closeSendEmailPanel.onClick.RemoveAllListeners();
    }


    private async System.Threading.Tasks.Task ResetPasswordMethodAsync()
    {
        errorText.gameObject.SetActive(false);
        email = emailField.text;
        if (string.IsNullOrEmpty(email) || !firebaseHandler.ValidEmail(email))
        {
            errorText.text = notAnEmailMessage;
            StartCoroutine(ShowErrorMessage());
            return;
        }

        var auth = firebaseHandler.GetAuth();
        emailExists = false;
        try {

            await auth.FetchProvidersForEmailAsync(email).ContinueWith((emailCheckingTask) =>
            {
                List<string> allProviders = ((IEnumerable<string>)emailCheckingTask.Result).ToList();

                if (allProviders.Count > 0)
                {
                    emailExists = true;
                }
            });
        }
        catch ( Exception ex)
        {
            Debug.LogWarning(ex);
            errorText.text = errorMessage;
            StartCoroutine(ShowErrorMessage());
        }

        await TrySendEmailAsync(auth);
    }

    private async System.Threading.Tasks.Task TrySendEmailAsync(FirebaseAuth auth)
    {
        waitText.gameObject.SetActive(true);
        sendEmailBtn.gameObject.SetActive(false);
        if (emailExists)
        {
            await auth.SendPasswordResetEmailAsync(email);
            emailSentPanel.gameObject.SetActive(true);
            sendEmailPanel.gameObject.SetActive(false);
        }
        else
        {
            errorText.text = errorMessage;
            StartCoroutine(ShowErrorMessage());
        }

        waitText.gameObject.SetActive(false);
        sendEmailBtn.gameObject.SetActive(true);
    }

    private IEnumerator ShowErrorMessage()
    {
        yield return new WaitForSeconds(0.2f);
        errorText.gameObject.SetActive(true);
    }
}
