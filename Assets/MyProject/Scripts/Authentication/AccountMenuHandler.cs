﻿using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class AccountMenuHandler : MonoBehaviour
{

    //Delegate and events used to communicate with the UserDetails script
    public delegate void AuthenticationChange();

    //initialize the events to empty delegate in order to avoid null reference exception
    public static event AuthenticationChange OnSignedOut = delegate { };
    public static event AuthenticationChange OnSignedIn = delegate { };
    public static event AuthenticationChange OnRegistered = delegate { };

    #region Serialized Fields
    [SerializeField]
    private TextMeshProUGUI logInRequiredText;
    [SerializeField]
    private Canvas connectionProblemsCanvas;
    [SerializeField]
    private RectTransform signInPanel;
    [SerializeField]
    private RectTransform registerPanel;
    [SerializeField]
    private RectTransform signOutPanel;
    [SerializeField]
    private TMP_InputField passwordRegister;
    [SerializeField]
    private TMP_InputField emailRegister;
    [SerializeField]
    private TMP_InputField passwordSignIn;
    [SerializeField]
    private TMP_InputField emailSignIn;
    [SerializeField]
    private RectTransform mainMenu;
    [SerializeField]
    private Button registerBtn;
    [SerializeField]
    private Button logInBtn;
    [SerializeField]
    private Button goToSignIn;
    [SerializeField]
    private Button goToRegister;
    [SerializeField]
    private Button signOutBtn;
    #endregion 

    private Account account;
    private AccountFirebaseHandler firebaseHandler;
    public bool HasLoggedIn = false;

    [Inject]
    private void Init(Account injectedAccount, AccountFirebaseHandler injectedFirebaseHandler)
    {
        account = injectedAccount;
        firebaseHandler = injectedFirebaseHandler;
    }

    private void OnEnable()
    {
        RegisterButtonActions();

        //Check if the auth and user objects exist to communicate with the UserDetails
        //script whether the details should be displayed  
        if (!firebaseHandler.CheckIfSignedIn())
        {
            account.SetAccountDefault();
            OnSignedOut();
            ActivatePanel(signInPanel, registerPanel, signOutPanel);
        }
        else
        {
            account.SetAccount(firebaseHandler.GetUser().Email);
            OnSignedIn();
            //activate sign out panel and deactivate others
            ActivatePanel(signOutPanel, signInPanel, registerPanel);
        }

        AccountFirebaseHandler.OnSignedIn += HasSignedIn;
        AccountFirebaseHandler.OnSignedUp += HasSignedUp;
        AccountFirebaseHandler.OnErrorOccured += ErrorHasOccured;
    }

    private void OnDisable()
    {
        UnregisterButtonActions();
        AccountFirebaseHandler.OnSignedIn -= HasSignedIn;
        AccountFirebaseHandler.OnSignedUp -= HasSignedUp;
        AccountFirebaseHandler.OnErrorOccured -= ErrorHasOccured;
    }

    private void HasSignedIn()
    {
        if (firebaseHandler.GetAuth() != null)
        {
            Firebase.Auth.FirebaseUser user = firebaseHandler.GetUser();
            if (user != null)
            {
                logInRequiredText.gameObject.SetActive(false);
                account.SetAccount(user.Email);
                DeactivateProblemsCanvas();
                OnSignedIn();
                ActivatePanel(signOutPanel, signInPanel, registerPanel);
                GoToMainMenu();
            }
            else
            {
                ActivateProblemsCanvas();
            }
        }
        else
        {
            ActivateProblemsCanvas();
        }
    }

    private void HasSignedUp()
    {
        if (firebaseHandler.GetAuth() != null)
        {
            Firebase.Auth.FirebaseUser user = firebaseHandler.GetUser();
            if (user != null)
            {
                logInRequiredText.gameObject.SetActive(false);
                account.SetAccount(user.Email);
                DeactivateProblemsCanvas();
                OnRegistered();
                ActivatePanel(signOutPanel, registerPanel, signInPanel);
                GoToMainMenu();
            }
            else
            {
                ActivateProblemsCanvas();
            }
        }
        else
        {
            ActivateProblemsCanvas();
        }
    }

    private void ErrorHasOccured()
    {
        ActivateProblemsCanvas();
    }

    #region Firebase communication methods 

    private async System.Threading.Tasks.Task TrySignUpAsync(string email, string password)
    {
        await firebaseHandler.SignUpAsync(email, password);
    }

    private async System.Threading.Tasks.Task TrySignInAsync(string email, string password)
    {
        await firebaseHandler.SignInAsync(email, password);
    }

    private void SignOut()
    {
        firebaseHandler.GetAuth().SignOut();
        account.SetAccountDefault();
        OnSignedOut();
        ActivatePanel(signInPanel, registerPanel, signOutPanel);
    }
    #endregion

    private void RegisterButtonActions()
    {
        registerBtn.onClick.AddListener(() => TrySignUpAsync(emailRegister.text, passwordRegister.text));
        logInBtn.onClick.AddListener(() => TrySignInAsync(emailSignIn.text, passwordSignIn.text));
        //activate sign in panel, deactivate others
        goToSignIn.onClick.AddListener(() => ActivatePanel(signInPanel, registerPanel, signOutPanel));
        //activate register panel, deactivate others
        goToRegister.onClick.AddListener(() => ActivatePanel(registerPanel, signInPanel, signOutPanel));
        signOutBtn.onClick.AddListener(SignOut);
    }

    private void UnregisterButtonActions()
    {
        registerBtn.onClick.RemoveAllListeners();
        logInBtn.onClick.RemoveAllListeners();
        goToRegister.onClick.RemoveAllListeners();
        goToSignIn.onClick.RemoveAllListeners();
    }

    private void GoToMainMenu()
    {
        gameObject.SetActive(false);
        mainMenu.gameObject.SetActive(true);
    }

    private void ActivatePanel(RectTransform toActivate, RectTransform panel1, RectTransform panel2 )
    {
        panel1.gameObject.SetActive(false);
        panel2.gameObject.SetActive(false);
        toActivate.gameObject.SetActive(true);
    }

    private void ActivateProblemsCanvas()
    {
        logInRequiredText.gameObject.SetActive(false);
        if (!connectionProblemsCanvas.gameObject.activeInHierarchy)
        {
            connectionProblemsCanvas.gameObject.SetActive(true);
        }
    }

    private void DeactivateProblemsCanvas()
    {
        if (connectionProblemsCanvas.gameObject.activeInHierarchy)
        {
            connectionProblemsCanvas.gameObject.SetActive(false);
        }
    }

}
