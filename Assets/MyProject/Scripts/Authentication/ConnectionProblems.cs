﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionProblems : MonoBehaviour
{
    [SerializeField]
    private Button closeButton;

    private void Awake()
    {
        this.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        closeButton.onClick.AddListener(CloseMessage);
    }

    void OnDisable()
    {
        closeButton.onClick.RemoveAllListeners();
    }

    private void CloseMessage()
    {
        this.gameObject.SetActive(false);
    }

}
