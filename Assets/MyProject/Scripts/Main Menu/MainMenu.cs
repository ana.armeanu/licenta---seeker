﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static SoundManager;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI username;
    [SerializeField]
    private Button soloPlayBtn;
    [SerializeField]
    private Button multiPlayBtn;
    [SerializeField]
    private Button optionsBtn;
    [SerializeField]
    private Button accountBtn;
    [SerializeField]
    private Button LeaderBtn;
    [SerializeField]
    private List<Button> mainMenuButtons;
    [SerializeField]
    private Transform mainMenuPanel;
    [SerializeField]
    private Transform accountPanel;
    [SerializeField]
    private Transform leaderboardPanel;
    [SerializeField]
    private Transform optionsPanel;
    [SerializeField]
    private Transform soloPlayOptPanel;
    [SerializeField]
    private TextMeshProUGUI youNeedToSignIn;
    [SerializeField]
    private Transform signInNoticePanel;
    [SerializeField]
    private Button goToAccount;

    private void OnEnable()
    {
        youNeedToSignIn.gameObject.SetActive(false);

        string multiScene = "Launcher";

        foreach (Button btn in mainMenuButtons)
        {
            btn.onClick.AddListener(GoToMainPanel);
        }

        signInNoticePanel.gameObject.SetActive(false);
        soloPlayOptPanel.gameObject.SetActive(false);
        mainMenuPanel.gameObject.SetActive(true);
        signInNoticePanel.gameObject.SetActive(false);
        accountPanel.gameObject.SetActive(false);
        optionsPanel.gameObject.SetActive(false);
        leaderboardPanel.gameObject.SetActive(false);
        optionsBtn.onClick.AddListener(GoToOptionsPanel);
        accountBtn.onClick.AddListener(GoToAccountPanel);
        LeaderBtn.onClick.AddListener(GoToLeaderPanel);

        goToAccount.onClick.AddListener(GoToAccountPanel);
        soloPlayBtn.onClick.AddListener(ActivateSoloPlayOpt);
        multiPlayBtn.onClick.AddListener(() => LoadLevel(multiScene));

        PlaySoundLoop(SoundName.MenuMusic, gameObject);
    }

    private void OnDisable ()
    {
        goToAccount.onClick.RemoveAllListeners();
        optionsBtn.onClick.RemoveAllListeners();
        accountBtn.onClick.RemoveAllListeners();
        LeaderBtn.onClick.RemoveAllListeners();
        soloPlayBtn.onClick.RemoveAllListeners();
        multiPlayBtn.onClick.RemoveAllListeners();
    }

    private void ActivateSoloPlayOpt()
    {
        soloPlayOptPanel.gameObject.SetActive(true);
    }

    private bool CheckIfSignedIn()
    {
        if (string.Equals(username.text, "Not signed in"))
        {
            return false;
        }
        return true;
    }

    private void LoadLevel(string sceneName)
    {
        if (string.Equals(sceneName, "Launcher"))
        {
            if (CheckIfSignedIn())
            {
                SceneManager.LoadScene(sceneName);
            }
            else
            {
                GoToAccountPanel();
                youNeedToSignIn.gameObject.SetActive(true);
            }
        }
        else
        {
            SceneManager.LoadScene(sceneName);
        }
    }


    private void GoToOptionsPanel()
    {
        mainMenuPanel.gameObject.SetActive(false);
        optionsPanel.gameObject.SetActive(true);
        soloPlayOptPanel.gameObject.SetActive(false);
        signInNoticePanel.gameObject.SetActive(false);
    }
    private void GoToLeaderPanel()
    {
        mainMenuPanel.gameObject.SetActive(false);
        leaderboardPanel.gameObject.SetActive(true);
        soloPlayOptPanel.gameObject.SetActive(false);
        signInNoticePanel.gameObject.SetActive(false);
    }

    private void GoToAccountPanel()
    {
        youNeedToSignIn.gameObject.SetActive(false);
        mainMenuPanel.gameObject.SetActive(false);
        accountPanel.gameObject.SetActive(true);
        soloPlayOptPanel.gameObject.SetActive(false);
        signInNoticePanel.gameObject.SetActive(false);
    }

    private void GoToMainPanel()
    {
        mainMenuPanel.gameObject.SetActive(true);
        accountPanel.gameObject.SetActive(false);
        optionsPanel.gameObject.SetActive(false);
        leaderboardPanel.gameObject.SetActive(false);
        soloPlayOptPanel.gameObject.SetActive(false);
        signInNoticePanel.gameObject.SetActive(false);
    }
}
