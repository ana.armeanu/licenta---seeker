﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoloPlayOpt : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI username;
    [SerializeField]
    private Button loadLastGame;
    [SerializeField]
    private Button loadNewGame;
    [SerializeField]
    private Button loadWithoutSigningBtn;
    [SerializeField]
    private Transform signInNoticePanel;
    [SerializeField]
    private Button instructionsBtn;
    [SerializeField]
    private Button close;

    private bool usePlayerPrefs = true;

    // Start is called before the first frame update
    void OnEnable()
    {
        loadWithoutSigningBtn.onClick.AddListener(() => LoadLevelByIndex(false, usePlayerPrefs));
        loadLastGame.onClick.AddListener(() => LoadLevelByIndex(true, true));
        loadNewGame.onClick.AddListener(() => LoadLevelByIndex(true, false));
        instructionsBtn.onClick.AddListener(GoToInstructions);
        close.onClick.AddListener(CloseSoloApt);
    }

    // Update is called once per frame
    void OnDisable()
    {
        loadWithoutSigningBtn.onClick.RemoveAllListeners();
        loadLastGame.onClick.RemoveAllListeners();
        loadNewGame.onClick.RemoveAllListeners();
        instructionsBtn.onClick.RemoveAllListeners();
        close.onClick.RemoveAllListeners();
    }

    private void GoToInstructions()
    {
        SceneManager.LoadScene("Instructions Menu");
    }

    private void CloseSoloApt()
    {
        gameObject.SetActive(false);
    }

    private void LoadLevelByIndex(bool shouldCheckRegistered, bool usePlayerPrefsParam)
    {
        usePlayerPrefs = usePlayerPrefsParam;

        int sceneIndex = 1;
        if (usePlayerPrefsParam)
        {
            //Get the last played scene in solo mode 
            sceneIndex = PlayerPrefs.GetInt("PlayedScene");
            if (sceneIndex == default) sceneIndex = 1;
        }

        if (shouldCheckRegistered)
        {
            if (CheckIfSignedIn() == false)
            {
                signInNoticePanel.gameObject.SetActive(true);
                return;
            }
        }

        //if you finished the game, or you are at the first level, load the instructions scene first
        if (string.Equals(NameFromIndex(sceneIndex), "Water Level") || string.Equals(NameFromIndex(sceneIndex), "Final Scene"))
        {
            SceneManager.LoadScene("Instructions Scene");
        }
        else
        {
            SceneManager.LoadScene(sceneIndex);
        }
    }

    private string NameFromIndex(int BuildIndex)
    {
        string path = SceneUtility.GetScenePathByBuildIndex(BuildIndex);
        int slash = path.LastIndexOf('/');
        string name = path.Substring(slash + 1);
        int dot = name.LastIndexOf('.');
        return name.Substring(0, dot);
    }


    private bool CheckIfSignedIn()
    {
        if (string.Equals(username.text, "Not signed in"))
        {
            return false;
        }
        return true;
    }


}
