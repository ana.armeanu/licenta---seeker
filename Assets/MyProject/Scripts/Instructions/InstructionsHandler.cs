﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static SoloPlayLevelManager;

public class InstructionsHandler : MonoBehaviour
{
    [SerializeField]
    private RectTransform instructionsPanel;
    [SerializeField]
    private RectTransform instructionsPanel2;
    [SerializeField]
    private RectTransform introductionPanel;
    [SerializeField]
    private RectTransform crystalCanvas;
    [SerializeField]
    private Button SkipBtn;
    [SerializeField]
    private Button NextBtnInstructions;
    [SerializeField]
    private Button NextBtnInstructions2;
    [SerializeField]
    private Button NextBtnIntroduction;
    // Start is called before the first frame update
    private void OnEnable()
    {
        crystalCanvas.gameObject.SetActive(false);
        introductionPanel.gameObject.SetActive(true);
        instructionsPanel.gameObject.SetActive(false);
        instructionsPanel2.gameObject.SetActive(false);
        SkipBtn.onClick.AddListener(LoadFirstScene);
        NextBtnInstructions2.onClick.AddListener(LoadFirstScene);
        NextBtnIntroduction.onClick.AddListener(LoadNext);
        NextBtnInstructions.onClick.AddListener(LoadNext);

        Instance.LevelLoaded(Level.Instructions, 4);
    }

    private void OnDisable()
    {
        SkipBtn.onClick.RemoveAllListeners();
        NextBtnInstructions.onClick.RemoveAllListeners();
        NextBtnIntroduction.onClick.RemoveAllListeners();
    }

    private void LoadFirstScene()
    {
        SceneManager.LoadScene("Water Level");
    }

    private void LoadNext()
    {
        if (instructionsPanel.gameObject.activeInHierarchy == false)
        {
            introductionPanel.gameObject.SetActive(false);
            instructionsPanel.gameObject.SetActive(true);
        }
        else
        {
            instructionsPanel.gameObject.SetActive(false);
            instructionsPanel2.gameObject.SetActive(true);
            crystalCanvas.gameObject.SetActive(true);
        }
    }
}
