﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FadeInText : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI[] phrases;

    private void Start()
    {
        if (phrases != null)
        {
            foreach (var t in phrases)
            {
                t.gameObject.SetActive(false);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        StartCoroutine(CallTexts());
    }

    private IEnumerator CallTexts()
    {
        for(int i = 0; i < phrases.Length; i++)
        {
            phrases[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(3);
        }
    }
}
