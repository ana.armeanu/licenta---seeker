﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundInfo
{
    private static float musicVolume;
    private static float fxVolume;

    public static float GetMusicVolume()
    {
        return musicVolume;
    }

    public static float GetFXVolume()
    {
        return fxVolume;
    }

    public static void SetMusicVolume(float mVol)
    {
        musicVolume = mVol;
    }

    public static void SetFXVolume(float fxVol)
    {
        fxVolume = fxVol;
    }
}
