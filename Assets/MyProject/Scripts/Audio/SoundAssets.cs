﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundAssets : MonoBehaviour
{
    private static SoundAssets instance;

    public static SoundAssets Instance
    {
        get
        {
            if (instance == null)
                instance = Instantiate(Resources.Load<SoundAssets>("SoundAssets"));
            return instance;
        }
    }

    public List<SoundClass> SoundClassList; 

    [System.Serializable]
    public class SoundClass
    {
        public SoundManager.SoundName soundName;
        public AudioClip audioClip;
    }
}
