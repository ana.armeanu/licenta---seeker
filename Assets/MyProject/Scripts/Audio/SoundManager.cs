﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class SoundManager
{
    public enum SoundName
    {
        PlayerMove,
        PlayerAttack,
        PlayerDeath,
        PlayerGotHit,
        EnemyMove, 
        EnemyAttack,
        EnemyDeath,
        EnemyGotHit,
        MenuMusic,
        LevelMusic,
        Level2Music,
        Level3Music,
        QuizMusic,
        GameOver,
        OpponentJoined,
        RightAnswer, 
        WrongAnswer, 
        LauncherMusic,
        InstructionsMusic,
        EndSceneMusic,
        StartedReading,
        CollectedCrystal,
        GateNewLevel, 
        SoloWon,
        SoloGameOver
    }
    private static float musicVolume = 1;
    private static float fxVolume = 1;


    public static List<AudioSource> fxSourceList = new List<AudioSource>();
    private static List<AudioSource> musicSourceList = new List<AudioSource>();

    public static AudioSource PlaySoundLoop(SoundName s, GameObject objSource = null, float pitch = 1f)
    {
        AudioSource audioSource = GetAudioSource(s, objSource);
        audioSource.volume = musicVolume;
        audioSource.loop = true;
        audioSource.pitch = pitch;
        audioSource.clip = GetSound(s);
        audioSource.Play();
        musicSourceList.Add(audioSource);
        return audioSource;
    }

    public static AudioSource PlaySoundOnce(SoundName s, GameObject objSource = null, float pitch = 1f)
    {
        AudioSource audioSource = GetAudioSource(s, objSource);
        audioSource.volume = fxVolume;
        audioSource.pitch = pitch;
        audioSource.PlayOneShot(GetSound(s));
        fxSourceList.Add(audioSource);
        return audioSource;
    }

    public static AudioSource PlaySoundOnceDelay(SoundName s, GameObject objSource = null,float delay = 0f, float pitch = 1f)
    {
        AudioSource audioSource = GetAudioSource(s, objSource);
        audioSource.volume = fxVolume;
        audioSource.loop = false;
        audioSource.pitch = pitch;
        audioSource.PlayDelayed(delay);
        fxSourceList.Add(audioSource);
        return audioSource;
    }

    private static AudioSource GetAudioSource(SoundName s, GameObject objSource = null)
    {
        AudioSource audioSource;
        if (objSource == null)
        {
            GameObject soundGameObj = new GameObject("Sound " + s);
            audioSource = soundGameObj.AddComponent<AudioSource>();
        }
        else
        {
            audioSource = objSource.GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = objSource.AddComponent<AudioSource>();
            }
        }

        return audioSource;
    }

    private static AudioClip GetSound(SoundName s)
    {
        AudioClip audio = SoundAssets.Instance.SoundClassList.Find(x => x.soundName == s).audioClip;
        if (audio == null)
        {
            Debug.LogWarning("The audio clip is null");
        }
        return audio;
    }

    public static void ChangeMusicVolume(float value)
    {
        musicVolume = value;

        musicSourceList.RemoveAll(x => x == null);
        foreach (AudioSource s in musicSourceList)
        {
            s.volume = musicVolume;
        }
    }

    public static void ChangeFXVolume(float value)
    {
        fxVolume = value;
        fxSourceList.RemoveAll(x => x == null);
        foreach (AudioSource s in fxSourceList)
        {
            s.volume = fxVolume;
        }
    }

    public static void StopSound(AudioSource source)
    {
        source.Stop();
    }
}
