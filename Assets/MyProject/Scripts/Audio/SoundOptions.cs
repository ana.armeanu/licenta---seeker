﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundOptions : MonoBehaviour
{
    [SerializeField]
    private Slider musicVolume;
    [SerializeField]
    private Slider fxVolume;

    private void Awake()
    {
        musicVolume.value = 1;
        fxVolume.value = 1;
    }

    private void OnEnable()
    {
        musicVolume.onValueChanged.AddListener( delegate { UpdateMusicVolume(); } );
        fxVolume.onValueChanged.AddListener(delegate { UpdateFxVolume(); });
    }

    void Start()
    {
        SetVolume();
    }

    private void OnDisable()
    {
        musicVolume.onValueChanged.RemoveAllListeners();
        fxVolume.onValueChanged.RemoveAllListeners();
    }

    private void SetVolume()
    {
        if (PlayerPrefs.HasKey("MusicVolume") && PlayerPrefs.HasKey("FXVolume"))
        {
            musicVolume.value = PlayerPrefs.GetFloat("MusicVolume");
            fxVolume.value = PlayerPrefs.GetFloat("FXVolume");
        }
        else
        {
            musicVolume.value = 1;
            fxVolume.value = 1;
            PlayerPrefs.SetFloat("MusicVolume", 1);
            PlayerPrefs.SetFloat("FXVolume", 1);
        }
    }

    private void UpdateMusicVolume()
    {
        PlayerPrefs.SetFloat("MusicVolume", musicVolume.value);
        SoundManager.ChangeMusicVolume(musicVolume.value);
    }

    private void UpdateFxVolume()
    {
        PlayerPrefs.SetFloat("FXVolume", fxVolume.value);
        SoundManager.ChangeFXVolume(fxVolume.value);
    }
}
