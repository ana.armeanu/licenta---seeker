﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickHandler : MonoBehaviour
{
    [SerializeField]
    private FixedJoystick movementJoystick;

    [SerializeField]
    private FixedJoystick cameraJoystick;


    private void OnEnable()
    {
        movementJoystick.enabled = true;
        cameraJoystick.enabled = true;
        //movementJoystick.SnapY = false;

    }

    private void OnDisable()
    {
        movementJoystick.enabled = false;
        cameraJoystick.enabled = false;
       // movementJoystick.SnapY = false;

    }
}
