using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{

    public override void InstallBindings()
    {
        InstallTextResources();

        Container.Bind<Account>().AsSingle().NonLazy();
        Container.Bind<AccountFirebaseHandler>().AsSingle().NonLazy();
    }

    private void InstallTextResources()
    {
        Container.Bind<WaterTextResource>().AsSingle().NonLazy();
        Container.Bind<EarthTextResource>().AsSingle().NonLazy();
        Container.Bind<AstronomyTextResource>().AsSingle().NonLazy();
        Container.Bind<QAResource>().AsSingle().NonLazy();
    }
}
