﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InstructionsSceneHandler : MonoBehaviour
{
    [SerializeField]
    private Button nextBtn;
    [SerializeField]
    private List<Button> backBtnList;
    [SerializeField]
    private RectTransform firstPanel;
    [SerializeField]
    private RectTransform secondPanel;
    [SerializeField]
    private RectTransform secondPanelCanvas;

    private void OnEnable()
    {
        SoundManager.PlaySoundLoop(SoundManager.SoundName.InstructionsMusic);
        firstPanel.gameObject.SetActive(true);
        secondPanel.gameObject.SetActive(false);
        secondPanelCanvas.gameObject.SetActive(false);
        nextBtn.onClick.AddListener(GetSecondPanel);
        foreach (Button btn in backBtnList)
        {
            btn.onClick.AddListener(GoBack);
        }
    }

    private void OnDisable()
    {
        nextBtn.onClick.RemoveAllListeners();
        foreach (Button btn in backBtnList)
        {
            btn.onClick.RemoveAllListeners();
        }
    }

    private void GetSecondPanel()
    {
        firstPanel.gameObject.SetActive(false);
        secondPanel.gameObject.SetActive(true);
        secondPanelCanvas.gameObject.SetActive(true);
    }

    private void GoBack()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
