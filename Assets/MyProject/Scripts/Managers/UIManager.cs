﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public delegate void Death();

    public static event Death HasDied;

    public void RaiseEvents()
    {
        HasDied();
    }
}
