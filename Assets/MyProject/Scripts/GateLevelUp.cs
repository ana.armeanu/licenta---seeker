﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using static SoundManager;

public class GateLevelUp : MonoBehaviour
{
    [SerializeField]
    private string NextLevelName;
    [SerializeField]
    private Camera GateCamera;
    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private TextMeshProUGUI restrictLevelUp;
    [SerializeField]
    private ParticleSystem customParticleSystem;
    [SerializeField]
    private RectTransform mainCanvas;



    private ParticleSystem activatedGate;
    private bool canLevelUp = false;
    private bool showLevelUpUI = false;
    //in case there is already a text fading out, don't show it again on collisin
    private bool showText = true;

    private void OnEnable()
    {
        ButtonInteraction.HasFinishedReading += TryShowGate;
        GateCamera.gameObject.SetActive(false);
        restrictLevelUp.gameObject.SetActive(false);
        activatedGate = GetComponent<ParticleSystem>();
        LevelHandler.CanLevelUp += CanLevelUpUI;
    }

    private void OnDisable()
    {
        LevelHandler.CanLevelUp -= CanLevelUpUI;
        ButtonInteraction.HasFinishedReading -= TryShowGate;
    }

    private void TryShowGate()
    {
        if (showLevelUpUI)
        {
            mainCanvas.gameObject.SetActive(false);
            customParticleSystem.Play();
            Camera playerCamera = Player.GetComponent<PlayerCameraController>().GetActiveCamera();
            playerCamera.gameObject.SetActive(false);
            GateCamera.gameObject.SetActive(true);
            StartCoroutine(WaitToReturnToGame(playerCamera));
            //there is no reason to show it again
            showLevelUpUI = false;
        }
    }

    private void CanLevelUpUI()
    {
        showLevelUpUI = true;
        canLevelUp = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!string.Equals(collision.gameObject.tag, "Player"))
            return;
        if (canLevelUp)
        {
            PlaySoundOnce(SoundName.GateNewLevel);
            StartCoroutine(WaitForGateSound());
        }
        else
        {
            if (showText)
            {
                Color c = restrictLevelUp.color;
                c.a = 1;
                restrictLevelUp.gameObject.SetActive(true);
                restrictLevelUp.color = c;
                showText = false;
                StartCoroutine(FadeTextOut(restrictLevelUp));
            }
        }
    }

    private IEnumerator WaitForGateSound()
    {
        yield return new WaitForSeconds(0.7f);
        SoloPlayLevelManager.Instance.LoadNextLevel();
    }

    private IEnumerator FadeTextOut(TextMeshProUGUI t)
    {
        yield return new WaitForSeconds(0.5f);
        for (float i = 1; i > 0; i -= 0.05f)
        {
            Color c = t.color;
            c.a = i;
            t.color = c;
            yield return new WaitForSeconds(0.05f);
        }

        t.gameObject.SetActive(false);
        showText = true;
    }

    private IEnumerator WaitToReturnToGame(Camera playerCamera)
    {
        yield return new WaitForSeconds(3);
        mainCanvas.gameObject.SetActive(true);
        GateCamera.gameObject.SetActive(false);
        playerCamera.gameObject.SetActive(true);
    }
}
