﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemAttack : EnemyAttack
{
    [SerializeField]
    private ParticleSystem attackParticles;

    protected override IEnumerator AttackOverTime()
    {
        anim.SetBool("isAttacking", true);
        //wait for the attack animation to finish
        yield return new WaitForSeconds(0.5f);
        targetHandler.DecreaseHealth(damage);
        anim.SetBool("isAttacking", false);
        attackParticles.Play();

        //wait for the new attack to begin
        yield return new WaitForSeconds(1.5f);

        if (canAttack)
        {
            StartCoroutine(AttackOverTime());
        }
        else
        {
            coroutineRunning = false;
        }
    }
}
