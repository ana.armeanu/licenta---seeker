﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : DamageGiver
{
    [SerializeField]
    protected float startAttackDistance = 10;
    protected Transform target;
    protected PlayerStatsHandler targetHandler;
    protected bool canAttack;
    //number of seconds between consecutive enemy attacks
    protected float seconds;
    protected bool coroutineRunning;
    protected Animator anim;

    protected void Start()
    {
        damage = 10;
        canAttack = false;
        coroutineRunning = false;
        target = GameObject.FindGameObjectWithTag("Player").transform;
        targetHandler = target.GetComponent<PlayerStatsHandler>();
        anim = GetComponent<Animator>();
    }

    protected void Update()
    {
        float dist = Vector3.Distance(target.position, this.transform.position);

        if (dist <= startAttackDistance)
        {
            if (targetHandler != null)
            {
                canAttack = true;
                if (coroutineRunning == false)
                {
                    StartCoroutine(AttackOverTime());
                    coroutineRunning = true;
                }
            }
        }
        else
        {
            canAttack = false;
        }
    }

    protected virtual IEnumerator AttackOverTime()
    {
        anim.SetBool("isAttacking", true);

        //wait for the attack animation to finish
        yield return new WaitForSeconds(0.5f);
        targetHandler.DecreaseHealth(damage);
        anim.SetBool("isAttacking", false);

        //wait for the new attack to begin
        yield return new WaitForSeconds(1.5f);
        if (canAttack)
        {
            StartCoroutine(AttackOverTime());
        }
        else
        {
            coroutineRunning = false;
        }
    }
}
