﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFollow : MonoBehaviour
{
    [SerializeField]
    private float startFollowingDistance = 100;
    //NavMeshAgent already has a stopping distance from the target(when it gets as close to the target)
    private Transform target;
    private NavMeshAgent navComponent;
    private Animator anim;
    private bool isWalking;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        navComponent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        isWalking = false;
    }

    private void OnEnable()
    {
        PlayerMovement.PlayerHasMoved += ResetAgentTarget;
    }

    private void ResetAgentTarget()
    {
        float dist = Vector3.Distance(target.position, this.transform.position);

        if (dist <= startFollowingDistance)
        {
            navComponent.isStopped = false;
            navComponent.SetDestination(target.position);
            isWalking = true;
        }
    }

    private void Update()
    {
        float dist = Vector3.Distance(target.position, this.transform.position);

        if (dist <= startFollowingDistance)
        {
            if (target != null && isWalking == false)
            {
                navComponent.SetDestination(target.position);
            }
            else
            {
                target = GameObject.FindGameObjectWithTag("Player").transform;
            }
            isWalking = true;
        }
        else
        {
            if (isWalking)
            {
                navComponent.isStopped = true;
            }
            isWalking = false;
        }

        if (dist <= navComponent.stoppingDistance)
        {
            isWalking = false;
            transform.LookAt(target.transform.position);
        }

        if (anim != null)
        {
            anim.SetBool("isWalking", isWalking);
        }
    }

    private void OnDisable()
    {
        PlayerMovement.PlayerHasMoved -= ResetAgentTarget;
    }
}
