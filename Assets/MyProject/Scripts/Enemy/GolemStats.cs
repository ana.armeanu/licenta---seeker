﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemStats : EnemyStatsHandler
{
    protected override void Start()
    {
        base.Start();
        percentDamageTaken = 0.5f;//50%
    }
}
