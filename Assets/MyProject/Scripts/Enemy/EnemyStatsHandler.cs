﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//abstract class so that it cannot stay by itself on an object
public abstract class EnemyStatsHandler : BaseStatsHandler
{
    [SerializeField]
    protected ParticleSystem deathParticles;
    protected Animator anim;
    protected float percentDamageTaken;

    protected virtual void Start()
    {
        anim = GetComponent<Animator>();
    }

    protected void Update()
    {
        if (healthValue <= 0)
        {
            DeathProcedures();
        }
    }

    public override void DecreaseHealth(float damage)
    {
        SoundManager.PlaySoundOnce(SoundManager.SoundName.EnemyGotHit, gameObject);
        StartCoroutine(ResetDamageAnimation());
        base.DecreaseHealth((damage * percentDamageTaken));
    }

    private IEnumerator ResetDamageAnimation()
    {
        anim.SetBool("gotHit", true);
        yield return new WaitForSeconds(1f);
        anim.SetBool("gotHit", false);
    }

    private void DeathProcedures()
    {
        this.gameObject.SetActive(false);
        deathParticles.Play();
    }
}
