﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Zenject;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region private Serialize Fields
    [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
    [SerializeField]
    private byte maxPlayersPerRoom = 2;
    [SerializeField]
    private RectTransform connectingText;
    [SerializeField]
    private RectTransform disconnectedText;
    [SerializeField]
    private Button backButton;
    [SerializeField]
    private Button playButton;
    [SerializeField]
    private RectTransform playPanel;
    #endregion

    #region Private Fields

    private Account accountHandler;
    private string gameVersion = "1";
    private bool isConnecting;
    
    #endregion

    [Inject]
    private void Construct(Account injectedAccount)
    {
        accountHandler = injectedAccount;
    }

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        // make sure PhotonNetwork.LoadLevel() can be used on the master client and all clients in the same room sync their level automatically 
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        backButton.onClick.AddListener(GoToMainMenu);
        playButton.onClick.AddListener(Connect);
        connectingText.gameObject.SetActive(false);
        disconnectedText.gameObject.SetActive(false);
        SoundManager.PlaySoundLoop(SoundManager.SoundName.LauncherMusic, gameObject);
    }

    public override void OnDisable()
    {
        base.OnDisable();
        backButton.onClick.RemoveAllListeners();
        playButton.onClick.RemoveAllListeners();
    }

    #endregion

    #region Public Methods

    public void Connect()
    {
        isConnecting = true;
        playPanel.gameObject.SetActive(false);
        connectingText.gameObject.SetActive(true);
        disconnectedText.gameObject.SetActive(false);
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            PhotonNetwork.LocalPlayer.NickName = accountHandler.Username;
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    #endregion

    #region
    private void GoToMainMenu()
    {
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }
    #endregion

    #region MonoBehaviourPunCallbacks Callbacks

    public override void OnConnectedToMaster()
    {
        Debug.Log("Launcher: OnConnectedToMaster() was called by PUN");
        // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
        if (isConnecting)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        //TO DO client conectat
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        playPanel.gameObject.SetActive(false);
        playButton.gameObject.SetActive(false);
        connectingText.gameObject.SetActive(false);
        disconnectedText.gameObject.SetActive(true);
        Debug.LogWarningFormat("Launcher: OnDisconnected() was called with reason {0}", cause);
        //TO DO client deconectat (posibila problema de net)
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");

        // #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            Debug.Log("We load the 'Room for 2' ");


            // #Critical
            // Load the Room Level.
            PhotonNetwork.LoadLevel("Room for 2");
        }
    }
    #endregion
}
