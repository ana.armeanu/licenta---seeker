﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIAnswerStatus : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI playerCorrectAnswers;
    [SerializeField]
    private TextMeshProUGUI playerWrongAnswers;
    [SerializeField]
    private TextMeshProUGUI opponentCorrectAnswers;
    [SerializeField]
    private TextMeshProUGUI opponentWrongAnswers;

    private void Increment(string toBeSearched, TextMeshProUGUI textUGUI)
    {
        string s = textUGUI.text;
        string numberString = s.Substring(s.IndexOf(toBeSearched) + toBeSearched.Length);
        int currentNumber = Int32.Parse(numberString);

        textUGUI.text = toBeSearched + (currentNumber + 1).ToString();
    }

    private void IncrementPlayerWrong()
    {
        Increment("Wrong: ", playerWrongAnswers);
    }

    private void IncrementPlayerRight()
    {
        Increment("Right: ", playerCorrectAnswers);
    }

    private void IncrementOpponentWrong()
    {
        Increment("Wrong: ", opponentWrongAnswers);
    }

    private void IncrementOpponentRight()
    {
        Increment("Right: ", opponentCorrectAnswers);
    }

    public void IncrementMasterRightAnswers()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            IncrementPlayerRight();
            SoundManager.PlaySoundOnce(SoundManager.SoundName.RightAnswer, gameObject);
        }
        else
        {
            IncrementOpponentRight();
        }
    }

    public void IncrementMasterWrongAnswers()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            IncrementPlayerWrong();
            SoundManager.PlaySoundOnce(SoundManager.SoundName.WrongAnswer, gameObject);
        }
        else
        {
            IncrementOpponentWrong();
        }
    }

    public void IncrementNormalWrongAnswers()
    {
        if (PhotonNetwork.IsMasterClient == false)
        {
            IncrementPlayerWrong();
            SoundManager.PlaySoundOnce(SoundManager.SoundName.WrongAnswer, gameObject);
        }
        else
        {
            IncrementOpponentWrong();
        }
    }

    public void IncrementNormalRightAnswers()
    {
        if (PhotonNetwork.IsMasterClient == false)
        {
            IncrementPlayerRight();
            SoundManager.PlaySoundOnce(SoundManager.SoundName.RightAnswer, gameObject);
        }
        else
        {
            IncrementOpponentRight();
        }
    }
}
