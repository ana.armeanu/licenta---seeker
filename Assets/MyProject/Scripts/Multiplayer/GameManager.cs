﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Realtime;
using TMPro;
using System;

public class GameManager : MonoBehaviourPunCallbacks
{
    #region Private Serialized Fields

    [SerializeField]
    private Button leaveRoomButton;
    [SerializeField]
    private TextMeshProUGUI currentUsername;
    [SerializeField]
    private TextMeshProUGUI opponentUsername;
    [SerializeField]
    private Button goBackToLobby;
    [SerializeField]
    private RectTransform waitingOpponentCanvas;
    #endregion


    #region MonoBehaviour

    public override void OnEnable()
    {
        base.OnEnable();
        leaveRoomButton.onClick.AddListener(LeaveRoom);
        goBackToLobby.onClick.AddListener(LeaveRoom);
        currentUsername.text = PhotonNetwork.LocalPlayer.NickName;
        WaitingCanvasOnMaster();
        SoundManager.PlaySoundLoop(SoundManager.SoundName.QuizMusic, gameObject);

        foreach (var name in PhotonNetwork.PlayerList)
        {
            if (!String.Equals(name.NickName, PhotonNetwork.LocalPlayer.NickName))
            {
                opponentUsername.text = name.NickName;
            }
        }
    }


    private void WaitingCanvasOnMaster()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            waitingOpponentCanvas.gameObject.SetActive(true);
        }
    }


    private void OnDestroy()
    {
        leaveRoomButton.onClick.RemoveAllListeners();
    }

    #endregion

    #region Photon Callbacks

    public override void OnDisconnected(DisconnectCause cause)
    {
        SceneManager.LoadScene("Launcher");
        Debug.LogWarningFormat("Launcher: OnDisconnected() was called with reason {0}", cause);
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("Launcher");
    }

    public override void OnJoinedRoom()
    {
        currentUsername.text = PhotonNetwork.LocalPlayer.NickName;
        foreach (var name in PhotonNetwork.PlayerList)
        {
            if (!String.Equals(name.NickName, PhotonNetwork.LocalPlayer.NickName))
            {
                opponentUsername.text = name.NickName;
            }
        }
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting
        opponentUsername.text = other.NickName;

        if (PhotonNetwork.IsMasterClient)
        {
            waitingOpponentCanvas.gameObject.SetActive(false);
        }
    }

    public override void OnPlayerLeftRoom(Player other)
    {
        //close the room so no other players can join
        PhotonNetwork.CurrentRoom.MaxPlayers = 1;
    }

    #endregion

    #region Public Methods

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    #endregion

    #region Private Methods

    private void LoadArena()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
        }
        Debug.LogFormat("PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);
        PhotonNetwork.LoadLevel("Room for 2");
    }

    #endregion
}
