﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerConnection : MonoBehaviour
{

    #region Private Fields

    private AccountFirebaseHandler accountHandler;
    #endregion

    #region Private Methods

    [Inject]
    private void Initialize(AccountFirebaseHandler injectedHandler)
    {
        accountHandler = injectedHandler;
    }

    #endregion
    // Start is called before the first frame update
    private void Start()
    {
        //PhotonNetwork.NickName = accountHandler.Username;
    }

    private void OnEnable()
    {
        StartCoroutine(WaitForNickName());
    }

    private IEnumerator WaitForNickName()
    {
        yield return new WaitForSeconds(0.5f);
        string email = accountHandler.GetUser().Email;
        PhotonNetwork.NickName = email.Substring(0, email.IndexOf("@"));
    }
}
