﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class QuizLogic : MonoBehaviourPunCallbacks, IOnEventCallback
{
    //how many questions there are on a round
    private readonly int secondsPerRound = 8;
    private readonly int totalQuestionsRound = 4;
    private readonly int xpRightAnswer = 5;
    private readonly int xpGameWon = 10;//even for equalities

    #region Serialized Fields
    [SerializeField]
    private RectTransform mainPanel;
    [SerializeField]
    private RectTransform opponentLeftCanvas;
    [SerializeField]
    private RectTransform gameFinishedPanel;
    [SerializeField]
    private TextMeshProUGUI question;
    [SerializeField]
    private Button btnA;
    [SerializeField]
    private Button btnB;
    [SerializeField]
    private Button btnC;
    [SerializeField]
    private Button btnD;
    [SerializeField]
    private TextMeshProUGUI time;
    [SerializeField]
    private TextMeshProUGUI xpOnOppLeft;
    [SerializeField]
    private Image opponentWrong;
    [SerializeField]
    private Image currentWrong;
    [SerializeField]
    private Image opponentRight;
    [SerializeField]
    private Image currentRight;
    #endregion

    #region Private Fields
    private UIAnswerStatus uiAnswerStatus;
    private QAResource qaResource;
    private int rightAnswer;
    private List<Button> choiceButtons;
    private bool currentAnswerIsRight;
    private bool masterAnsweredRightValue;
    private bool normalAnsweredRightValue;
    private readonly byte MasterHasAnsweredEvent = 0;
    private readonly byte NormalHasAnsweredEvent = 1;
    private readonly byte ShouldShowAnswer = 2;
    private readonly byte GameFinishedEvent = 3;
    private bool gameFinished = false;
    private int xpGained = 3; //each player receives 3 xp points for joining the game
    private bool won = false;
    private bool tie = false;
    private int currentQuestion = 0;

    private int rightAnswersMaster = 0;
    private int rightAnswersNormal = 0;
    private Account account;
    #endregion

    [Inject]
    private void Initialize(QAResource injectedResource, Account injectedAccount)
    {
        qaResource = injectedResource;
        account = injectedAccount;
    }

    #region Pun methods overridden

    public override void OnJoinedRoom()
    {
        DisableAnswerFeedback();
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            StartGame();
        }
    }

    public override void OnPlayerLeftRoom(Player other)
    {
        if (!gameFinished)
        {
            gameFinished = true;
            opponentLeftCanvas.gameObject.SetActive(true);
            //If the opponent leaves, the current player wins.
            xpGained += xpGameWon;
            xpOnOppLeft.text = $"You gained {xpGained} experience points.";
            account.UpdateXP(xpGained);
            SoundManager.PlaySoundOnce(SoundManager.SoundName.GameOver);
        }
    }

    #endregion

    #region Monobehaviour methods

    public override void OnEnable()
    {
        base.OnEnable();

        uiAnswerStatus = GetComponent<UIAnswerStatus>();

        //make sure that there aren't duplicate events
        PhotonNetwork.RemoveCallbackTarget(this);
        PhotonNetwork.AddCallbackTarget(this);

        opponentLeftCanvas.gameObject.SetActive(false);
        mainPanel.gameObject.SetActive(true);
        gameFinishedPanel.gameObject.SetActive(false);

        btnA.onClick.AddListener(()=>ChoiceButtonAction(0));
        btnB.onClick.AddListener(() => ChoiceButtonAction(1));
        btnC.onClick.AddListener(() => ChoiceButtonAction(2));
        btnD.onClick.AddListener(() => ChoiceButtonAction(3));

        CreateButtonslist();
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        foreach (var btn in choiceButtons)
        {
            btn.onClick.RemoveAllListeners();
        }
    }

    #endregion

    private void CreateButtonslist()
    {
        choiceButtons = new List<Button>();
        choiceButtons.Add(btnA);
        choiceButtons.Add(btnB);
        choiceButtons.Add(btnC);
        choiceButtons.Add(btnD);
    }

    /// <summary>
    /// Extract the question and answers. Shuffle the answer.
    /// Set the index of the current right answer.
    /// Call the RPC to Update the UI
    /// </summary>
    private void ShowQuiz()
    {
        currentQuestion++;
        if (currentQuestion > totalQuestionsRound) return;
        QuizQA q1 = qaResource.GetRandomInfo();
        List<string> ansList = new List<string>();
        ansList.Add(q1.Answer);
        ansList.Add(q1.Wrong1);
        ansList.Add(q1.Wrong2);
        ansList.Add(q1.Wrong3);
        for (var i = 0; i < 4; ++i)
        {
            var r = Random.Range(i, 4);
            var tmp = ansList[i];
            ansList[i] = ansList[r];
            ansList[r] = tmp;
        }

        for (var i = 0; i < 4; ++i)
        {
            if (string.Equals(q1.Answer, ansList[i]))
            {
                rightAnswer = i;
                CallSetRightAnswerRPC(i);
                break;
            }
        }

        CallUpdateQARPC(q1.Question, ansList[0], ansList[1], ansList[2], ansList[3]);
    }

    /// <summary>
    /// Hide the answer feedback images;
    /// Make all the current answers false
    /// Show the first question
    /// Start the round
    /// </summary>
    private void StartGame()
    {
        CallDisableAnswerFeedbackRPC();
        MakeAnswersFalse();
        ShowQuiz();
        StartCoroutine(StartRound());
    }

    private void MakeAnswersFalse()
    {
        normalAnsweredRightValue = false;
        masterAnsweredRightValue = false;
        currentAnswerIsRight = false;
    }

    #region Round Logic

    /// <summary>
    /// For each question in the round, recolor all buttons and call TimeLapse
    /// </summary>
    private IEnumerator StartRound()
    {
        CallRecolorAllButtonsRPC();
        for (int i = 1; i <= totalQuestionsRound; i++)
        {
           // CallRecolorAllButtonsRPC();
            StartCoroutine(TimeLapse());
            yield return new WaitForSeconds(12);
            if (gameFinished)
            {
                break;
            }
        }
        //Game finished
        if (!gameFinished)
        {
            GameFinishedMethod();
        }
    }

    private void SetFinishedStatus()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (rightAnswersMaster > rightAnswersNormal) won = true;
            if (rightAnswersMaster == rightAnswersNormal) tie = true;
        }
        else
        {
            if (rightAnswersNormal > rightAnswersMaster) won = true;
            if (rightAnswersNormal == rightAnswersMaster) tie = true;
        }

        //Add the experience points for winning
        if (won || tie)
        {
            xpGained += xpGameWon;
        }
    }

    private void UIGameFinished()
    {
        mainPanel.gameObject.SetActive(false);
        gameFinishedPanel.gameObject.SetActive(true);
        if (won)
        {
            gameFinishedPanel.transform.Find("Win text").gameObject.SetActive(true);
        }
        else
        {
            if (tie)
            {
                gameFinishedPanel.transform.Find("Equality text").gameObject.SetActive(true);
            }
            else
            {
                gameFinishedPanel.transform.Find("Lost text").gameObject.SetActive(true);
            }
        }

        string xpText = $"You gained {xpGained} experience points.";
        gameFinishedPanel.transform.Find("XP gained").GetComponent<TextMeshProUGUI>().text = xpText;
        account.UpdateXP(xpGained);
        SoundManager.PlaySoundOnce(SoundManager.SoundName.GameOver);
    }

    private void GameFinishedMethod()
    {
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        SendOptions sendOptions = new SendOptions { Reliability = true };
        PhotonNetwork.RaiseEvent(GameFinishedEvent, true, raiseEventOptions, sendOptions);
    }

    //Update the Time Countdown text
    //After time is up, call the ShowAnswer event, through the method
    // and prepare the new question
    private IEnumerator TimeLapse()
    {
        int i = secondsPerRound;
        while (i >= 0)
        {
            CallUpdateTimeRPC(i);
            i--;
            if (i < 0)
            {
                ShouldShowAnswerMethod();
            }
            yield return new WaitForSeconds(1);
        }
        if (!gameFinished)
        {
            TimeOutNext();
        }
    }

    private void TimeOutNext()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            CallRecolorAllButtonsRPC();
            ShowQuiz();
        }
    }

    //Called by the event on both players
    private IEnumerator ShowAnswerFeedback()
    {
        if (gameFinished) yield break;
        if (PhotonNetwork.IsMasterClient)
        {
            MasterHasAnsweredMethod();
        }
        else
        {
            NormalHasAnsweredMethod();
        }

        yield return new WaitForSeconds(0.5f);

        if (PhotonNetwork.IsMasterClient)
        {
            MasterAnswerFeedback();
        }
        else
        {
            NormalAnswerFeedback();
        }

        MakeAnswersFalse();

        yield return new WaitForSeconds(2f);

        DisableAnswerFeedback();
    }
    #endregion

    private void ChoiceButtonAction(int btnNumber)
    {
        ColorYourChoice(btnNumber);
        if (rightAnswer == btnNumber)
        {
            //right choice
            currentAnswerIsRight = true;
        }
        else
        {
            //wrong choice
            currentAnswerIsRight = false;
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

        if (eventCode == NormalHasAnsweredEvent)
        {
            bool data = (bool)photonEvent.CustomData;

            normalAnsweredRightValue = data;
            if (normalAnsweredRightValue)
            {
                rightAnswersNormal++;
                uiAnswerStatus.IncrementNormalRightAnswers();
            }
            else
            {
                uiAnswerStatus.IncrementNormalWrongAnswers();
            }
        }

        if (eventCode == MasterHasAnsweredEvent)
        {
            bool data = (bool)photonEvent.CustomData;

            masterAnsweredRightValue = data;
            if (masterAnsweredRightValue)
            {
                rightAnswersMaster++;
                uiAnswerStatus.IncrementMasterRightAnswers();
            }
            else
            {
                uiAnswerStatus.IncrementMasterWrongAnswers();
            }
        }

        if (eventCode == ShouldShowAnswer)
        {
            ColorRightChoice();
            StartCoroutine(ShowAnswerFeedback());
        }

        if (eventCode == GameFinishedEvent)
        {
            gameFinished = true;
            SetFinishedStatus();
            UIGameFinished();
        }
    }

    #region Helper Methods

    private void MasterAnswerFeedback()
    {
        if (masterAnsweredRightValue)
        {
            currentRight.gameObject.SetActive(true);
            //Add the xp points for a right answer because the player is master
            xpGained += xpRightAnswer;
        }
        else
        {
            currentWrong.gameObject.SetActive(true);
        }

        if (normalAnsweredRightValue)
        {
            opponentRight.gameObject.SetActive(true);
        }
        else
        {
            opponentWrong.gameObject.SetActive(true);
        }
    }

    private void NormalAnswerFeedback()
    {
        if (masterAnsweredRightValue)
        {
            opponentRight.gameObject.SetActive(true);
        }
        else
        {
            opponentWrong.gameObject.SetActive(true);
        }

        if (normalAnsweredRightValue)
        {
            currentRight.gameObject.SetActive(true);
            //Add the xp points for a right answer because the player is normal
            xpGained += xpRightAnswer;
        }
        else
        {
            currentWrong.gameObject.SetActive(true);
        }
    }

    private void ColorYourChoice(int btnNumber)
    {
        RecolorMyChoices(btnNumber);
        Color c = new Color(1.0f, 0.64f, 0.0f);//orange
        c.a = 0.7f;
        choiceButtons[btnNumber].gameObject.GetComponentInChildren<Image>().color = c;
    }

    private void ColorRightChoice()
    {
        Button btn = choiceButtons[rightAnswer];
        Color c = Color.green;
        c.a = 0.4f;
        btn.gameObject.GetComponentInChildren<Image>().color = c;

        MakeButtonsInteractable(false);
    }

    private void MakeButtonsInteractable(bool interactable)
    {
        foreach (Button b in choiceButtons)
        {
            b.interactable = interactable;
        }
    }
    #endregion

    #region Raise Event Methods

    private void ShouldShowAnswerMethod()
    {
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // Everyone receives this event but the local player
        SendOptions sendOptions = new SendOptions { Reliability = true };
        PhotonNetwork.RaiseEvent(ShouldShowAnswer, true, raiseEventOptions, sendOptions);  
    }

    private void NormalHasAnsweredMethod()
    {
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // Everyone receives this event but the local player
        SendOptions sendOptions = new SendOptions { Reliability = true };
        PhotonNetwork.RaiseEvent(NormalHasAnsweredEvent, currentAnswerIsRight, raiseEventOptions, sendOptions);
    }

    private void MasterHasAnsweredMethod()
    {
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // Everyone receives this event but the local player
        SendOptions sendOptions = new SendOptions { Reliability = true };
        PhotonNetwork.RaiseEvent(MasterHasAnsweredEvent, currentAnswerIsRight, raiseEventOptions, sendOptions);
    }

    #endregion

    #region RPCs

    [PunRPC]
    private void RecolorAllChoices()
    {
        foreach (var btn in choiceButtons)
        {
            Color c = Color.white;
            c.a = 0.4f;
            btn.gameObject.GetComponentInChildren<Image>().color = c;
        }

        MakeButtonsInteractable(true);
    }

    //Only recolor the buttons not pressed
    private void RecolorMyChoices(int idx)
    {
        for(int i = 0; i< choiceButtons.Count; i++)
        {
            if(i != idx)
            {
                Color c = Color.white;
                c.a = 0.4f;
                choiceButtons[i].gameObject.GetComponentInChildren<Image>().color = c;
            }
        }
    }

    [PunRPC]
    private void SetRightAnswer(int n)
    {
        rightAnswer = n;
    }

    [PunRPC]
    private void DisableAnswerFeedback()
    {
        opponentWrong.gameObject.SetActive(false);
        opponentRight.gameObject.SetActive(false);
        currentWrong.gameObject.SetActive(false);
        currentRight.gameObject.SetActive(false);
    }

    [PunRPC]
    private void UpdateTime(int t)
    {
        time.text = t.ToString();
    }

    [PunRPC]
    private void UpdateQA(string q, string txtA, string txtB, string txtC, string txtD)
    {
        question.text = q;
        btnA.GetComponent<TextMeshProUGUI>().text = "A. " + txtA;
        btnB.GetComponent<TextMeshProUGUI>().text = "B. " + txtB;
        btnC.GetComponent<TextMeshProUGUI>().text = "C. " + txtC;
        btnD.GetComponent<TextMeshProUGUI>().text = "D. " + txtD;
    }

    #endregion

    #region Call RPCs

    private void CallSetRightAnswerRPC(int i)
    {
        PhotonView photonView2 = PhotonView.Get(this);
        photonView2.RPC("SetRightAnswer", RpcTarget.All, i);
    }

    private void CallUpdateQARPC(string q, string a1, string a2, string a3, string a4)
    {
        PhotonView photonView = PhotonView.Get(this);
        photonView.RPC("UpdateQA", RpcTarget.All, q, a1, a2, a3, a4);
    }

    private void CallDisableAnswerFeedbackRPC()
    {
        PhotonView photonView = PhotonView.Get(this);
        photonView.RPC("DisableAnswerFeedback", RpcTarget.All);
    }

    private void CallRecolorAllButtonsRPC()
    {
        if (currentQuestion == totalQuestionsRound) return;
        PhotonView photonView = PhotonView.Get(this);
        photonView.RPC("RecolorAllChoices", RpcTarget.All);
    }

    private void CallUpdateTimeRPC(int i)
    {
        PhotonView photonView = PhotonView.Get(this);
        photonView.RPC("UpdateTime", RpcTarget.All, i);
    }

    #endregion
}
