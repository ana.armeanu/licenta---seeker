﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseStatsHandler : MonoBehaviour
{
    [SerializeField]
    private Image healthBar;

    protected float healthValue = 100;
    protected readonly float multiplier = 0.01f;

    private void OnEnable()
    {
        healthValue = 100;
        UpdateHealthBar();
    }

    public virtual void DecreaseHealth(float damage)
    {
        healthValue -= damage;
        UpdateHealthBar();
    }

    protected void UpdateHealthBar()
    {
        healthBar.fillAmount = Mathf.Max(0, healthValue * multiplier);
    }
}
