﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static SoloPlayLevelManager;

//Level 2
public class EarthLevelHandler : LevelHandler
{
    private void Awake()
    {
        minInfoNb = 5;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        Instance.LevelLoaded(Level.EarthLevel, sceneIndex);
    }
}