﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardHandler : MonoBehaviour
{
    private class LeaderInfo
    {
        public string username;
        public int xp;
    }
    [SerializeField]
    private Vector2 firstRecordPosition;
    [SerializeField]
    private float recordHeight;
    [SerializeField]
    private float recordSpacing;
    [SerializeField]
    private Button btn;
    [SerializeField]
    private RectTransform panel;

    private List<LeaderInfo> leaderInfo;
    private LeaderboardRecordPool recordPool;

    void Start()
    {
        leaderInfo = new List<LeaderInfo>();
    }

    private void OnEnable()
    {
        recordPool = GetComponent<LeaderboardRecordPool>();
        GetLeadersList();
        //btn.onClick.AddListener(GetLeadersList);
    }


    public void GetLeadersList()
    {
        FirebaseDatabase.DefaultInstance
                .GetReference("users")
                .OrderByChild("Experience")
                .LimitToLast(5)
                .GetValueAsync()
                .ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Debug.Log("Fail To Load");
                    }
                    else if (task.IsCompleted)
                    {
                      DataSnapshot snapshot = task.Result;
                        leaderInfo.Clear();
                        foreach (DataSnapshot h in snapshot.Children)
                        {
                            LeaderInfo info = new LeaderInfo();
                            info.username = h.Child("Username").Value.ToString();
                            info.xp = Int32.Parse(h.Child("Experience").Value.ToString());
                            leaderInfo.Add(info);
                        }

                        leaderInfo.Reverse();
                        ArrangeUI();
                    }
                }
            , System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
    }

    private void ArrangeUI()
    {
        int size = leaderInfo.Count;

        for (int i = 0; i < size; i++)
        {
            GameObject g = recordPool.SpawnFromPool(firstRecordPosition - Vector2.up * i * recordSpacing, panel);
            //g.transform.parent = panel;
            g.transform.Find("Rank").gameObject.GetComponent<TextMeshProUGUI>().text = $"{i + 1}.";
            g.transform.Find("UserInfo").gameObject.GetComponent<TextMeshProUGUI>().text = leaderInfo[i].username;
            g.transform.Find("ExperienceInfo").gameObject.GetComponent<TextMeshProUGUI>().text = "Experience: " + leaderInfo[i].xp.ToString();
        }
    }
}
