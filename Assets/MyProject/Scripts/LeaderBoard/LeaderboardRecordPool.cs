﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardRecordPool : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;
    [SerializeField]
    private int size = 5;

    private Queue<GameObject> objectPool;

    private void Start()
    {
        objectPool = new Queue<GameObject>();

        for (int i = 0; i < size; i++)
        {
            GameObject obj = Instantiate(prefab);
            obj.SetActive(false);
            objectPool.Enqueue(obj);
        }
    }

    public GameObject SpawnFromPool(Vector2 position, RectTransform parent)
    {
        GameObject spawnedObj = objectPool.Dequeue();
        spawnedObj.transform.SetParent(parent, false);
        spawnedObj.SetActive(true);
        spawnedObj.GetComponent<RectTransform>().anchoredPosition = position;

        objectPool.Enqueue(spawnedObj);
        return spawnedObj;
    }

    [System.Serializable]
    private class Pool
    {
        public string objectName;
        public GameObject prefab;
        public int number; //number of elements to be instantiated at start
    }

}
