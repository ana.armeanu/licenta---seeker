﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelHandler : MonoBehaviour
{
    [SerializeField]
    protected TextMeshProUGUI minInfo;
    [SerializeField]
    protected TextMeshProUGUI collectedInfo;

    //how many pieces of information need to be read for each level in order to be able to pass it
    protected int minInfoNb;
    protected int collectedInfoNb = 0;
    //if the CanLevelUp event has been called, there is no need to call it again
    protected bool hasAnnounced;

    public static Action CanLevelUp = delegate { };

    protected virtual void OnEnable()
    {
        hasAnnounced = false;
        BaseCollectableInfo.ReadingNewCollectable += PlayerReadNewInfo;
        minInfo.text = $"/{minInfoNb}";
        collectedInfo.text = $"{collectedInfoNb}";
    }
        
    //Depending on how many pieces of information the player has interacted with, 
    //this method decides if the level can be passed
    protected bool TryPassLevel()
    {
        return (minInfoNb >= collectedInfoNb);
    }

    protected void PlayerReadNewInfo()
    {
        collectedInfoNb++;
        collectedInfo.text = collectedInfoNb.ToString();
        if (minInfoNb == collectedInfoNb && !hasAnnounced)
        {
            CanLevelUp();
            hasAnnounced = true;
        }
    }

    protected void OnDisable()
    {
        BaseCollectableInfo.ReadingNewCollectable -= PlayerReadNewInfo;
    }
}
