﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstronomyTextResource : BaseTextResource
{

    public AstronomyTextResource() : base()
    {
        infoPath = "AstronomyFacts";
        SetAllInfo();
    }
}

