﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthTextResource : BaseTextResource
{

    public EarthTextResource() : base()
    {
        infoPath = "EarthFacts";
        SetAllInfo();
    }
}

