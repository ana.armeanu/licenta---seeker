﻿using Collectables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class WaterTextResource : BaseTextResource
{
   
    public WaterTextResource() : base()
    {
        infoPath = "WaterFacts";
        SetAllInfo();
    }
}
