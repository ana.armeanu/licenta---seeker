﻿using Collectables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTextResource
{
    protected List<string> unreadTexts = new List<string>();
    protected List<string> readTexts = new List<string>();
    protected string infoPath;

    protected void SetAllInfo()
    {
        unreadTexts = InformationFileReader.ReadPiecesOfInfo(infoPath);
    }

    public string GetRandomInfo()
    {
        if (unreadTexts.Count > 0)
        {
            System.Random rnd = new System.Random();
            int idx = rnd.Next(0, unreadTexts.Count);
            string show = unreadTexts[idx];
            readTexts.Add(unreadTexts[idx]);
            unreadTexts.RemoveAt(idx);
            return show;
        }
        return null;
    }

}
