﻿using Collectables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ALevelCrystal : CrystalCollectable
{
    private void Start()
    {
        mana = 10;
        health = 15;
        xpPoints = 2;
    }
}
