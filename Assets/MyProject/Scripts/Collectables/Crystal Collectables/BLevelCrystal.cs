﻿using Collectables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BLevelCrystal : CrystalCollectable
{
    private void Start()
    {
        mana = 15;
        health = 30;
        xpPoints = 4;
    }
}
