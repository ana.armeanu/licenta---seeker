﻿using Collectables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CLevelCrystal : CrystalCollectable
{
    private void Start()
    {
        mana = 20;
        health = 50;
        xpPoints = 5;
    }
}
