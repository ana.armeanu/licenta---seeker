﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static SoundManager;

namespace Collectables
{
    public abstract class CrystalCollectable : MonoBehaviour, ICollectable
    {
        protected int mana;
        protected int health;
        protected int xpPoints;
        protected GameObject collidedPlayer;
        protected Account account;

        public void DeactivateCollectable()
        {
            GetComponent<Transform>().gameObject.SetActive(false);
        }

        [Inject]
        protected void Init(Account injectedAccount)
        {
            account = injectedAccount;
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                collidedPlayer = other.gameObject;
                DeactivateCollectable();
                AddXP();
                AddMana();
                AddHealth();
                PlaySoundOnce(SoundName.CollectedCrystal);
            }
        }

        public void AddMana()
        {
            collidedPlayer.GetComponent<PlayerStatsHandler>().AddMana(mana);
        }

        public void AddHealth()
        {
            collidedPlayer.GetComponent<PlayerStatsHandler>().AddHealth(health);
        }

        public void AddXP()
        {
            account.UpdateXP(xpPoints);
        }
    }
}
