﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public interface ICollectable
    {
        void AddXP();
    }
}