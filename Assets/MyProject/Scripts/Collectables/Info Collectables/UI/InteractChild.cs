﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InteractChild : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI tmpUGUI;

    public void SetText(string t)
    {
        tmpUGUI.text = t;
        tmpUGUI.ForceMeshUpdate();
    }
}
