﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInteraction : MonoBehaviour
{
    [SerializeField]
    private Button closeButton;
    [SerializeField]
    private Canvas playerInputCanvas;

    public static Action HasFinishedReading = delegate { };

    //easier syntax for events
    public static Action FinishedReadingCollectable = delegate { };

    private void Start()
    {
        closeButton.onClick.AddListener(CloseInfoUI);
    }

    private void OnEnable()
    {
        StartCoroutine(ShowCloseButton());
    }

    private void CloseInfoUI()
    {
        FinishedReadingCollectable();
        this.gameObject.SetActive(false);
        HasFinishedReading();
    }

    //Wait for 3 seconds (time for the player to read) and then show the button 
    public IEnumerator ShowCloseButton()
    {
        closeButton.gameObject.SetActive(false);
        yield return new WaitForSeconds(3);
        closeButton.gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        closeButton.onClick.RemoveAllListeners();
    }
}
