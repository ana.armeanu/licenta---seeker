﻿using Collectables;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using static SoundManager;

[RequireComponent(typeof(Collider))]
public abstract class BaseCollectableInfo : MonoBehaviour, ICollectable
{
    [SerializeField]
    protected GameObject InformationUIPrefab;
    [SerializeField]
    protected Button readButton;
    [SerializeField]
    protected RectTransform readButtonPanel;
    [SerializeField]
    protected GameObject particleParent;

    protected int xpPoints;
    protected Account account;
    protected BaseTextResource resource;
    protected string text = null;
    protected readonly string nameTMP = "TextMeshProText";
    protected GameObject instanceInformationUI;
    protected bool hasRead = false;
    //easier syntax for events
    public static Action ReadingNewCollectable = delegate { };

    [Inject]
    protected void Init(Account injectedAccount)
    {
        account = injectedAccount;
    }

    protected void OnCollisionEnter(Collision other)
    {
        if (hasRead == false)
        {
            if (other.gameObject.tag == "Player")
            {
                readButtonPanel.gameObject.SetActive(true);
                readButton.gameObject.SetActive(true);
                readButton.onClick.AddListener(LoadInfo);
            }
        }
    }

    protected void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            readButton.onClick.RemoveListener(LoadInfo);
            readButton.gameObject.SetActive(false);
            readButtonPanel.gameObject.SetActive(false);
        }
    }

    private void LoadInfo()
    {
        if (string.IsNullOrEmpty(text))
        {
            SetInformation();
        }
        if (instanceInformationUI == null)
        {
            ReadingNewCollectable();
            instanceInformationUI = Instantiate(InformationUIPrefab);
            SetInnerText(instanceInformationUI);
        }
        //StartedReadingCollectable();
        instanceInformationUI.SetActive(true);
        StopParicles();
        hasRead = true;
        //Get xp points for reading (each time)
        AddXP();
        PlaySoundOnce(SoundName.StartedReading, gameObject);
        
    }

    private void StopParicles()
    {
        if (!hasRead)
        {
            ParticleSystem particles = particleParent.gameObject.GetComponent<ParticleSystem>();
            particles.Stop();
        }
    }

    protected void SetInnerText(GameObject source)
    {
        InteractChild child = instanceInformationUI.GetComponent<InteractChild>();
        child.SetText(text);
    }


    protected void SetTMPText(TextMeshProUGUI tmpUGUI)
    {
        if (tmpUGUI != null)
        {
            tmpUGUI.text = text;
        }
        else
        {
            Debug.LogError("Couldn't find the prefab TMP child");
        }
    }

    public void SetInformation()
    {
        text = resource.GetRandomInfo();
    }

    public void AddXP()
    {
        account.UpdateXP(xpPoints);
    }

}
