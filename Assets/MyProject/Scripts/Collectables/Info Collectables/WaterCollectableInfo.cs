﻿using UnityEngine;
using Zenject;

public class WaterCollectableInfo : BaseCollectableInfo
{

    protected void Start()
    {
        xpPoints = 5;
    }

    //inject the water text resource singleton
    [Inject]
    protected void Init(WaterTextResource res)
    {
        resource = res;
    }
}
