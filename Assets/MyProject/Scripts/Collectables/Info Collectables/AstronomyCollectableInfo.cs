﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AstronomyCollectableInfo : BaseCollectableInfo
{
    protected void Start()
    {
        xpPoints = 10;
    }

    //inject the water text resource singleton
    [Inject]
    protected void Init(AstronomyTextResource res)
    {
        resource = res;
    }
}
