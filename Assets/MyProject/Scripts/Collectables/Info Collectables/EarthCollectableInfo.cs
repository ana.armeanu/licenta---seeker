﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EarthCollectableInfo : BaseCollectableInfo
{
    protected void Start()
    {
        xpPoints = 7;
    }


    //inject the water text resource singleton
    [Inject]
    protected void Init(EarthTextResource res)
    {
        resource = res;
    }
}