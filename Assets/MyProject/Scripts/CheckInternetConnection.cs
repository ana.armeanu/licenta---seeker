﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class CheckInternetConnection : MonoBehaviour
{
    [SerializeField]
    private Transform noInternetPrefab;

    private Animator animator;
    private bool isConnected;
    private bool mainCoroutineRunning;
    private bool secondCoroutineRunning;
    private void Awake()
    {
        isConnected = true;
        mainCoroutineRunning = false;
        secondCoroutineRunning = false;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public IEnumerator CheckConn(string url)
    {
        secondCoroutineRunning = true;
        Debug.Log("In");
        UnityWebRequest link = new UnityWebRequest(url);
        link.SendWebRequest();
        float elapsedTime = 0.0f;
        while (!link.isDone)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= 10.0f && link.downloadProgress <= 0.5)
            {
                break;
            }
            secondCoroutineRunning = false;
            yield return null;
        }

        if (!link.isDone || !string.IsNullOrEmpty(link.error))
        {
            noInternetPrefab.gameObject.SetActive(true);
            isConnected = false;
            yield break;
        }

        isConnected = true;
        noInternetPrefab.gameObject.SetActive(false);
        secondCoroutineRunning = false;
    }

    private void Update()
    {
       /* if (!mainCoroutineRunning)
        {
            StartCoroutine(Request());
        }*/
    }

    private IEnumerator Request()
    {
        mainCoroutineRunning = true;
        while (true)
        {
            if (secondCoroutineRunning == false)
            {
                secondCoroutineRunning = true;
                StartCoroutine(CheckConn("https://www.google.com"));
            }
            yield return new WaitForSeconds(2);
        }
    }
}
