﻿using System.Collections;
using System.Collections.Generic;
using Collectables;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


namespace Tests
{
    public class FileReaderTest
    {
        private readonly string dummyFilePath = Application.dataPath + "/MyProject/Scripts/Tests/DummyFile.txt";

        [Test]
        public void FileReaderSimplePasses()
        {
            List<string> expectedStrings = new List<string>() { "Information #1", "Information #2"};
            List<string> returnedStrings = InformationFileReader.ReadPiecesOfInfo(dummyFilePath);
            
            //check if the number of elements returned is the same
            Assert.AreEqual(expectedStrings.Count, returnedStrings.Count);

            //check if the strings are equal
            for (int i = 0; i < expectedStrings.Count; i++)
            {
                Assert.AreEqual(expectedStrings[i], returnedStrings[i]);
            }

        }
       
    }
}
