﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoseMenuButtons : MonoBehaviour
{
    [SerializeField]
    private Button playAgain;

    [SerializeField]
    private Button mainMenu;

    private void Start()
    {
        string levelName = SceneManager.GetActiveScene().name;
        playAgain.onClick.AddListener(()=>LoadScene(levelName));
        mainMenu.onClick.AddListener(() => LoadScene("Main Menu"));
    }

    private void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
