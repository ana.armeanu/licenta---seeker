﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using static SoloPlayLevelManager;

//Level 1 
public class WaterLevelHandler : LevelHandler
{
    private void Awake()
    {
        minInfoNb = 3;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        Instance.LevelLoaded(Level.WaterLevel, sceneIndex);
    }
}
